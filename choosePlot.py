import fourPlot_beta as four
import twoPlot_Beta as two
import threePlot_Beta as three

import argparse
import geofunc.geofunc as geo
import numpy as np
import pandas as pd 
from configparser import ConfigParser

file = 'config.ini'
config = ConfigParser()
config.read(file)


"""
These functions take the input from parser. 


threePlot, twoPlot, fourPlot get and set needed timeperiods and station data before continuing on to the 
plot function that creates and saves the figure as image on disk.


threeplot is only called if two time periods have been selected as input.
"""
def threePlot(x,y, range2X, range2Y, s_str):
    """
    Two time windows first one is for earthquakes and gps displasement (x,y)
    Second time window for earthquakes (range2X,range2X) shown as first figure
    """
    #Get coordinates from getCoord
    coord = getCoord(s_str)
    three.main(x,y, range2X, range2Y, coord[0], coord[1], coord[2], coord[3], s_str)


def twoPlot(x, y, s_str):
    
    #Get coordinates from getCoord
    coord = getCoord(s_str)
    two.main(x,y, coord[0], coord[1], coord[2], coord[3], s_str)
    

def fourPlot(x, y, s_str):
 
    #Get coordinates from getCoord
    coord = getCoord(s_str)
    
    #Get station names
    station = getStations(s_str)
    four.main(x,y, coord[0], coord[1], coord[2], coord[3], s_str, station[0], station[1])  
    


"""
function that returns the coordinates of sid( some gps station )
"""
def getStationCoord(sid):
    return geo.getStationCoordinates([sid])
    
    

"""
Not really useful, I´ĺl leave it here for now.
"""
def  setLatLonAuto(lat, lon):
    """
    Set the min max of lat and lon
    just a fixed square on the map around the gps station
    """
    min_lat = lat - 0.15
    max_lat = lat + 0.15
    min_lon = lon - 0.15
    max_lon = lon + 0.15
    return min_lat, max_lat, min_lon, max_lon


"""
TODO Perhaps, if needed...
"""
def setCoordsManually():
    print("This should print")
    
    
"""
#Get coordinates from config.ini with ConfigParser
#Coordinates are stored as strings.
"""
def getCoord(s_str):
    
    #get coordinates from config.ini file, with ConfigParser.
    min_lat = config.getfloat(s_str,'min_lat')
    max_lat = config.getfloat(s_str,'max_lat')
    min_lon = config.getfloat(s_str,'min_lon')
    max_lon = config.getfloat(s_str,'max_lon')
    
    coord = [min_lat, max_lat, min_lon, max_lon]
    return coord



"""
#Get the gas station names and/or hytro_station name 
#from config.ini with ConfigParser, stored as strings.
"""
def getStations(s_str):
    
    #get station names from config.ini file, with ConfigParser.
    station = config.get(s_str,'station')
    hytro_station = config.get(s_str,'hytro_station')
    
    stations = [station, hytro_station]
    return stations







