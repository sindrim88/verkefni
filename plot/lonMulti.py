#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function


from pylab import *
import numpy as np
import geofunc.geo as geo
import datetime

from matplotlib.colors import LinearSegmentedColormap

from matplotlib import cm
import matplotlib
matplotlib.pyplot.stem
matplotlib.axes.Axes.stem

def main(num1, num2,timeRange1,timeRange2):
    """
    """
    import cparser
    import logging

    from pathlib import Path
    from datetime import datetime as dt
    import gtimes.timefunc as tf
    #import timesmatplt.timesmatplt as tplt  # plot GPS data?
    import numpy as np # some simple calculations
    import geofunc.geo as geo
    import pandas as pd

    import geo_dataread.sil_read as gdrsil
    import matplotlib.dates as mdates  # better date/time axis ticks
    import timesmatplt.timesmatplt as tplt  # plot GPS data?

    # must register pandas datetime converter in matplotlib to avoid warning
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()
    
    
    from gtimes.timefunc import TimetoYearf
    
    import geo_dataread.gas_read as gdrgas
    import geo_dataread.sil_read as gdrsil
    import geo_dataread.hytro_read as gdrhyt
    import geo_dataread.gps_read as gdrgps
    
    import functions as ft
    
    dstr="%Y-%m-%d %H:%M:%S"
    dfstr="%Y-%m-%d"  # date format
    ### INPUT:
    sid = "GJOG"
    start = tf.currDatetime(num1)
    start_date = dt.strftime(start, dstr)
    
    
    
    if num2 == 0:
        end_date=dt.strftime(dt.now(), dstr)
    else:
        end_date = tf.currDatetime(num2)
    
    
    #Búa til annað tímabil fyrir skjálftagögn
    start2 = tf.currDatetime(timeRange1)
    start_date2 = dt.strftime(start2, dstr)
    
    if timeRange2 == 0:
        end_date2=dt.strftime(dt.now(), dstr)
    else:
        end_date2 = tf.currDatetime(timeRange2)
     
        
    logging_level=logging.INFO
    ## Everything else from here is automatic:

    StaPars = cparser.Parser()

    dt_start = tf.toDatetime(start_date,dstr) # datetime obj
    dt_end = tf.toDatetime(end_date,dstr)
    f_start = tf.currYearfDate(refday=dt_start) # float (fractional year)
    f_end = tf.currYearfDate(refday=dt_end)
    

    dt_start2 = tf.toDatetime(start_date2,dstr) # datetime obj
    dt_end2 = tf.toDatetime(end_date2,dstr)
    f_start2 = tf.currYearfDate(refday=dt_start2) # float (fractional year)
    f_end2 = tf.currYearfDate(refday=dt_end2)
   
       
    timestamp=dt.now()
    pend = timestamp + (dt_end2-dt_start2)/20 
    
    pre_path= "/mnt_data/"
    
    ## Get GPS data with some basic filtering
    gps_path="gpsdata/"
    #-#detrend_period=[TimetoYearf(2001,1,1), TimetoYearf(2010,1,1)] # FOR HVOL
    #-#detrend_line=[TimetoYearf(2010,6,1), TimetoYearf(2014,4,1)] # FOR HVOL
    ## Get GPS data with some basic filtering
    detrend_period=[TimetoYearf(2001,1,1), TimetoYearf(2019,1,1)]
    detrend_line=[TimetoYearf(2010,6,1), TimetoYearf(2019,4,1)]

    #GPS = gdrgps.read_gps_data(sid, Dir=pre_path+gps_path, start=dt_start, end=dt_end, detrend_period=detrend_period, 
    #               detrend_line=detrend_line, logging_level=logging_level)
    GPS = gdrgps.read_gps_data(sid, Dir=pre_path+gps_path, start=dt_start, end=dt_end, detrend_period=None, 
                   detrend_line=None, logging_level=logging_level)

    
    ## Get seismicity data
    sil_path="sildata/"
    #filtering the seismisity
    
    constr = {  #"start": start,     "end": end, 
                "min_lat": 66.00,  "max_lat":  66.6,
                "min_lon": -18.8, "max_lon": -17.2,
                "min_Q": 30.0, "max_Q": None,  
                #"min_depth": 15.0, "max_depth": None, 
                #"min_mlw": 1.0,  "max_mlw": None,
   }
    
        
    col_names = ['latitude', 'Dlatitude', 'longitude', 'Dlongitude', 
                    'depth', 'Ddepth', 'MLW', 'seismic_moment', 'cum_moment', 'Q']
    
    Seis = Seis2 = gdrsil.read_sil_data(start=dt_start, end=dt_end, base_path=pre_path+sil_path,  
            fread="default", aut=True, aut_per=2, rcolumns=col_names, cum_moment=True, logger='default', **constr)

    
    df1 = Seis['longitude'][0]
    print(df1)
    #53706
    
    length = len( Seis['longitude'])
    print(length)
    
    lst = [length]
    lstMLW = []
    date = []

    
    #for i in range(0, length):
        #lstMLW.append(Seis['MLW'][i])
  
    #print(lstMLW)
    loc = "IMO"
    if loc=="IMO":
        conn_dict={ 
               "dbname"    : "gas",
               "user"      : "gas_read",
               "password"  : "qwerty7",
               "host"      : "dev.psql.vedur.is",
               "port"      :  5432
               }
    
    elif loc=="home":
        # runn to make this work
        #ssh -f gpsops@rek.vedur.is -L 5431:dev.psql.vedur.is:5432 -N
        conn_dict={
                "dbname"    : "gas",
                "user"      : "gas_read",
                "password"  : "qwerty7",
                "host"      : "localhost",
                "port"      :  5431
                }
    
    
    ## Ploting
    import matplotlib.pyplot as plt

    xlsc=1.02 # x-axis limits w.r.t. data
    ylsc=1.05 # y-axis limits w.r.t. data (max.abs)
    moment_scale=1e14
 
    
    
    fig = ft.spltbbthreeFrame(Ylabel=None, Title=None)
    fig = ft.tsthreefigTickLabels(fig,period=(dt_end-dt_start),period2=(pend-dt_start2))

 
    # setting upp the axis stuf  
    seis_axis = fig.axes[0]
    seis_maxes = [seis_axis, seis_axis.twinx()]
    seis_axis.set_xlim([dt_start2,dt_end2])
      
    
    seis_axisDepth = fig.axes[1]
    seis_maxesDepth = [seis_axisDepth, seis_axisDepth.twinx()]
    seis_axisDepth.set_xlim([dt_start2,dt_end2])
    
    
    
    gps_axis = fig.axes[2]
    gps_maxes = [gps_axis, gps_axis.twinx(), gps_axis.twinx(),gps_axis.twinx()] 
    gps_axis.set_xlim([dt_start,dt_end])

    gps_maxes[2].spines['right'].set_position(("axes", 1.2))
    gps_maxes[2].set_frame_on(True)
    gps_maxes[2].patch.set_visible(True)
    
    
    
    
    fig.subplots_adjust(right=1.25)
    
    list2 = []
    list3 = []
    list4 = []
    list5 = []
    colors1 = []
    colors2 = []
    
    
    listMarkQ = []
    
    listDepth = []
    print(Seis.depth)
    for i in range(0,length):
        list2.append(4*3.5**Seis['MLW'][i])
        list3.append(Seis['MLW'][i])
        list4.append(Seis['longitude'][i])
        list5.append(Seis.index[i])
        listDepth.append(Seis['depth'][i])
            
        
        #Set  different marker for unvarified earthquakes.
        if Seis['Q'][i] >= 95:
                #Color code earthqukes.
            if Seis['MLW'][i] < 2:
                colors.append('green')
            elif Seis['MLW'][i] >= 2 and Seis['MLW'][i] < 3:
                colors.append('gold')
            elif Seis['MLW'][i] >= 3 and Seis['MLW'][i] < 4:
                colors.append('darkorange')
            elif Seis['MLW'][i] >= 4 and Seis['MLW'][i] < 5:
                colors.append('orangered')
            elif Seis['MLW'][i] >= 5 and Seis['MLW'][i] < 6:
                colors.append('red')
            elif Seis['MLW'][i] >= 6:
                colors.append('darkred')
            
        else:
           

                #Color code earthqukes.
                if Seis['MLW'][i] < 2:
                    colors.append('green')
                elif Seis['MLW'][i] >= 2 and Seis['MLW'][i] < 3:
                    colors.append('gold')
                elif Seis['MLW'][i] >= 3 and Seis['MLW'][i] < 4:
                    colors.append('darkorange')
                elif Seis['MLW'][i] >= 4 and Seis['MLW'][i] < 5:
                    colors.append('orangered')
                elif Seis['MLW'][i] >= 5 and Seis['MLW'][i] < 6:
                    colors.append('red')
                elif Seis['MLW'][i] >= 6:
                    colors.append('darkred')

    
    
    #To see what large earthquakes look like on the graph  
    """
    list2.append(4*3.5**7)
    list3.append(7)
    list4.append(-18)
    list5.append(Seis.index[length-1])
    colors.append('darkred')
    """
    
    seis_maxes[0].set_zorder(0) 
    ylim_magnitude=np.array([0, ylsc*max(Seis.MLW)])
    ylim_moment=np.array([0, ylsc*max(Seis.cum_moment)])
    
    
    seis_axis.text(0.01, 0.85, "Husavik", fontsize= 23, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
    seis_axis.text(0.01, 0.420, "Gjogurta", fontsize= 23, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
    seis_axis.text(0.01, 0.16, "Eyjafjardarall", fontsize= 23, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
    seis_axis.text(0.01, 0.62, "Flatey", fontsize= 23, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
    
    seis_maxes[0].hlines(y= -17.34, xmin=list5[0], xmax=list5[-1], color='gray', zorder=1)
    seis_maxes[0].hlines(y= -17.81, xmin=list5[0], xmax=list5[-1], color='gray', zorder=1)
    seis_maxes[0].hlines(y= -18.72, xmin=list5[0], xmax=list5[-1], color='gray', zorder=1)
    seis_maxes[0].hlines(y= -18.20, xmin=list5[0], xmax=list5[-1], color='gray', zorder=1)
    
    
    # How to mak this faster?
    
    #plot scattered earthquakes to handle different markers for varified and unvarified ertquakes.
    #     This is slow!!! 
    
    
    #seis_mar
    """
    for i in range(0,len(list2)):
            seis_maxes[0].scatter(list5[i], list4[i], alpha = 0.75, c=colors[i], label='Cumulative moment', zorder=4, s=list2[i], marker = listMarkQ[i])
    """
    
    Q = Seis[ Seis["Q"] > 95]
    Y = Seis[ Seis["Q"] <= 95]
    
    color = Seis[ Seis["Q"] > 95]
    
    seis_maxes[0].set_ylabel("Longitude", fontsize='large')
    
    #Faster but all the same markers 
    seis_maxes[0].scatter(Q.index, Q["longitude"], alpha = 0.75, c=colors, label='Cumulative moment', zorder=4, s=list2, marker = 'o')

 
    #seis_maxes[0].scatter(Y, list4, alpha = 0.75, c=colors, label='Cumulative moment', zorder=4, s=list2, marker = 'X')

    
    
    #seis_maxesDepth[0].set_zorder(1)
    
    #for i in range(0,len(list2)):
     #   seis_axisDepth[0].scatter(list5[i], list2[i], alpha = 0.75, c=colors[i], label='Cumulative moment', zorder=4, s=list2[i], marker = listMarkQ[i])
    
    
    
    ylim_magnitude=np.array([0, ylsc*max(Seis.MLW)])
    ylim_moment=np.array([0, ylsc*max(Seis.cum_moment)])
    seis_maxesDepth[0].set_xlim([start, pend])
    
    seis_maxesDepth[0].set_zorder(1)
    seis_maxesDepth[0].patch.set_visible(False)
    
    for i in range(0, len(list2)):
        seis_maxesDepth[0].scatter(list5[i], listDepth[i], alpha = 0.75, c=colors[i], label='Cumulative moment', s=list2[i], marker = listMarkQ[i])
    
    
    seis_maxesDepth[0].set_ylabel("Depth in km", fontsize='large')
    
    
    
    
    
    
    
    ## ploting GPS
    gps_maxes[0].set_zorder(1)
    gps_maxes[0].patch.set_visible(False)
    gps_maxes[0].errorbar(GPS.index, GPS.hlength, yerr=GPS.Dhlength, ls='none', ecolor='grey', elinewidth=0.2, 
                     capsize=0.6, capthick=0.2)
    gps_maxes[0].set_ylabel("Displacement\n[mm]", fontsize='large', labelpad=4)
    gps_maxes[0].plot_date(GPS.index, GPS.hlength, marker='o', markersize=8, markerfacecolor='r', 
               markeredgecolor='r', label='Horizontal displacemt')

    gps_maxes[0].errorbar(GPS.index, GPS.up, yerr=GPS.Dup, ls='none', ecolor='grey', elinewidth=0.2, 
                         capsize=0.6, capthick=0.2)
    #gps_maxes[1].set_ylabel("Vertical\ndisplacement [mm]", fontsize='large', labelpad=4)
    gps_maxes[0].plot_date(GPS.index, GPS.up, marker='o', markersize=8, markerfacecolor='black', 
               markeredgecolor='black', label='Vertical displacemt')
    gps_maxes[0].legend(loc=(0.05,0.67), numpoints=1, frameon=True, edgecolor="white", markerscale=2, framealpha=0.5)

    gps_infotext="GPS station {}".format(sid)
    gps_axis.text(0.5, 0.9, gps_infotext, fontsize=15, transform=gps_axis.transAxes, 
                  bbox=dict(facecolor='orange', alpha=0.1), ha='center' )
    
    
    # moment plot
    gps_maxes[1].set_zorder(3)
    gps_maxes[1].patch.set_visible(False)
    gps_maxes[1].set_ylabel("Seismic moment\n [$10^{14}\,$Nm]", fontsize='large')
    gps_maxes[1].plot(Seis.index, Seis.cum_moment/moment_scale, color="black", label='Cumulative moment', zorder=4)
    gps_maxes[1].fill_between(Seis.index, 0 , Seis.cum_moment/moment_scale, facecolor=(.8,.8,.8), alpha=0.4)
 
    
    #handles the color coded earthquakes by date
    cmap = matplotlib.colors.LinearSegmentedColormap.from_list("",["blue","orange","red"])
    
    length2 = len(Seis['MLW'])
    tempNum=0
    l = []
    listX = []
    listY = []
    markerSize = []

    for i in range(0, length2):
        tempNum = Seis['MLW'][i]
        if tempNum >= 4.5:
            l.append("*")
            listX.append(Seis.index[i])
            listY.append(Seis['MLW'][i])
            markerSize.append(650)

    
    gps_maxes[2].patch.set_visible(False)
    gps_maxes[2].set_zorder(5)
    
    #Plot starred and colorcoded earthquakes by date
    gps_maxes[2].scatter(Seis.index, Seis.MLW, c=Seis.index, cmap=cmap, s = 200,  marker =  "*",linewidth = 0.2, edgecolors='k')
    
    
    #This is drawing the starred quakes from the above for loop again, bigger earthquakes are drawn twice to handle 
    #   the lime green stars, inefficient, but they are not many.
    
    for i in range(0, len(l)):
        gps_maxes[2].scatter(listX[i], listY[i], c='lime', cmap=cmap, s = markerSize[i],  marker = l[i], edgecolors='k', linewidth = 0.4)
    
    
    ### I put the stems on another y axis, so the stems are the fourth y-axis and are draw before the "*" quakes.
    gps_maxes[3].set_zorder(1)
    gps_maxes[3].stem(Seis.index,  Seis.MLW, 'gray', markerfmt=" ", use_line_collection = True)
    
    
    #Set axis limit
    seis_maxes[0].set_ylim(-19,-17)
    seis_maxesDepth[0].set_ylim(30, 0)
    
    
    gps_maxes[-1].spines['right'].set_position(('axes', 1.045))
    gps_maxes[2].spines['right'].set_position(('axes', 1.045))
    
    
    gps_maxes[0].set_ylim(ymin=-30, ymax = 30)
    gps_maxes[1].set_ylim(*ylim_moment/moment_scale*1.5)
    gps_maxes[2].set_ylim(ymin = 0, ymax = 11)
    gps_maxes[3].set_ylim(ymin = 0, ymax = 11)
    
    
    print("done")
                                 
    # write to image file
    home = Path.home()
    print(home)

    
    print("Testing new beta")
    relpath = "multiplot/figures/"
    filebase = "GjogPlot"
    filename = home.joinpath(relpath,filebase).as_posix()
    #filename = "/home/gpsops/multiplot/figures/multiplot"

    
    tplt.saveFig(filename, "png", fig)
    tplt.saveFig(filename, "pdf", fig)
    
    del fig

    
    
if __name__=="__main__":
    main(-100, 0, -30, 0)

    
    