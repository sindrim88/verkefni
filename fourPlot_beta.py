#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function

import numpy as np
import geofunc.geo as geo


#"min_lat": 63.576,  "max_lat": 63.690,
#"min_lon": -19.269, "max_lon": -19.035,
                    
                    
                    
def main(num1, num2, minlat, maxlat, minlon, maxlon, sid, station, hytro_station):
    """
    """
    import cparser
    import logging

    from pathlib import Path
    from datetime import datetime as dt
    import gtimes.timefunc as tf
    #import timesmatplt.timesmatplt as tplt  # plot GPS data?
    import numpy as np # some simple calculations
    import geofunc.geo as geo
    import pandas as pd

    import geo_dataread.sil_read as gdrsil
    import matplotlib.dates as mdates  # better date/time axis ticks
    import timesmatplt.timesmatplt as tplt  # plot GPS data?

    # must register pandas datetime converter in matplotlib to avoid warning
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()
    
    
    from gtimes.timefunc import TimetoYearf
    
    import geo_dataread.gas_read as gdrgas
    import geo_dataread.sil_read as gdrsil
    import geo_dataread.hytro_read as gdrhyt
    import geo_dataread.gps_read as gdrgps
    
    import functions as ft
    
    dstr="%Y-%m-%d %H:%M:%S"
    dfstr="%Y-%m-%d"  # date format
    ### INPUT:
    #sid = "GFUM"
    start = tf.currDatetime(num1)
    start_date = dt.strftime(start, dstr)
    if num2 == 0:
        #dt.strftime(dt.now(), dstr)
        end_date=dt.strftime(dt.now(), dstr)
    else:
        end_date = tf.currDatetime(num2)
        #end_date=dt.strftime(num2, dstr)

    logging_level=logging.INFO
    ## Everything else from here is automatic:

    StaPars = cparser.Parser()

    dt_start = tf.toDatetime(start_date,dstr) # datetime obj
    dt_end = tf.toDatetime(end_date,dstr)
    f_start = tf.currYearfDate(refday=dt_start) # float (fractional year)
    f_end = tf.currYearfDate(refday=dt_end)

    #end = tf.currDatetime(-1500)

    pre_path= "/mnt_data/"
    
    ## Get GPS data with some basic filtering
    gps_path="gpsdata/"
    #-#detrend_period=[TimetoYearf(2001,1,1), TimetoYearf(2010,1,1)] # FOR HVOL
    #-#detrend_line=[TimetoYearf(2010,6,1), TimetoYearf(2014,4,1)] # FOR HVOL
    ## Get GPS data with some basic filtering
    detrend_period=[TimetoYearf(2001,1,1), TimetoYearf(2019,1,1)]
    detrend_line=[TimetoYearf(2010,6,1), TimetoYearf(2019,4,1)]

    #GPS = gdrgps.read_gps_data(sid, Dir=pre_path+gps_path, start=dt_start, end=dt_end, detrend_period=detrend_period, 
    #               detrend_line=detrend_line, logging_level=logging_level)
    GPS = gdrgps.read_gps_data(sid, Dir=pre_path+gps_path, start=dt_start, end=dt_end, detrend_period=None, 
                   detrend_line=None, logging_level=logging_level)

    
    ## Get seismicity data
    sil_path="sildata/"
    #filtering the seismisity
    
    
    constr = {  #"start": start,     "end": end, 
                "min_lat": minlat,  "max_lat":  maxlat,
                "min_lon": minlon, "max_lon": maxlon,
                #"min_depth": 15.0, "max_depth": None, 
                #"min_mlw": 1.0,  "max_mlw": None,
   }

    
    #constr = getConstr(min_lat, max_lat, min_lon, max_lon)
    
    
    
    col_names = ['latitude', 'Dlatitude', 'longitude', 'Dlongitude', 
                    'depth', 'Ddepth', 'MLW', 'seismic_moment', 'cum_moment']
    Seis = gdrsil.read_sil_data(start=dt_start, end=dt_end, base_path=pre_path+sil_path, 
                            fread="events.fps",   **constr)


    loc = "IMO"
    if loc=="IMO":
        conn_dict={ 
               "dbname"    : "gas",
               "user"      : "gas_read",
               "password"  : "qwerty7",
               "host"      : "dev.psql.vedur.is",
               "port"      :  5432
               }
    
    elif loc=="home":
        # runn to make this work
        #ssh -f gpsops@rek.vedur.is -L 5431:dev.psql.vedur.is:5432 -N
        conn_dict={
                "dbname"    : "gas",
                "user"      : "gas_read",
                "password"  : "qwerty7",
                "host"      : "localhost",
                "port"      :  5431
                }
    
    
    
    #station="lagu"
    device="crowcon"
    
    #station="hekla"
    #device="multigas"
    
    fname = "{0:s}_{1:s}.p.gz".format(station,device)
    dsource="DBASE"
    GAS = gdrgas.read_gas_data(station, device, start=dt_start, end=dt_end, 
                             dsource=dsource, wrfile=False, fname=fname, with_excluded=True, 
                             id_observation_start=False, conn_dict=conn_dict, logging_level=logging_level)
    
    ## Get hytrological data
    #hytro_station="v089"
    hytro_path="hytrodata/" 
    Hytro = gdrhyt.read_hytrological_data(hytro_station,start=dt_start, end=dt_end, frfile=False, fname="hytro-data.pik", 
                                 wrfile=False, base_path=pre_path+hytro_path, logging_level=logging_level )

    
    #print(data)
    print(station)
    ## Ploting
    import matplotlib.pyplot as plt

    xlsc=1.02 # x-axis limits w.r.t. data
    ylsc=1.05 # y-axis limits w.r.t. data (max.abs)
    moment_scale=1e14

    
    fig = ft.spltbbfourFrame(Ylabel=None, Title=None)
    fig = ft.tsfourfigTickLabels(fig,period=(dt_end-dt_start))

    # setting upp the axis stuff
    seis_axis = fig.axes[0] 
    seis_maxes = [seis_axis, seis_axis.twinx()]
    
    gps_axis = fig.axes[1]
    gps_maxes = [gps_axis]
    
    hytro_axis = fig.axes[2]
    hytro_maxes = [hytro_axis, hytro_axis.twinx(), hytro_axis.twinx()]
    hytro_maxes[-1].spines['right'].set_position(('axes', 1.08))
    hytro_maxes[-1].set_frame_on(True)
    hytro_maxes[-1].patch.set_visible(False)

    gas_axis = fig.axes[3]
    gas_maxes = [gas_axis, gas_axis.twinx()]
    #gas_maxes[-1].spines['right'].set_position(('axes', 1.08))
    #gas_maxes[-1].set_frame_on(True)
    #gas_maxes[-1].patch.set_visible(False)
    
    fig.subplots_adjust(right=0.75)

    
    seis_axis.set_xlim([dt_start,dt_end])
    gps_axis.set_xlim([dt_start,dt_end])
    hytro_axis.set_xlim([dt_start,dt_end])
    gas_axis.set_xlim([dt_start,dt_end])

    def plotSeis():
        
        ## ploting seismic
        ylim_magnitude=np.array([0, ylsc*max(Seis.MLW)])
        ylim_moment=np.array([0, ylsc*max(Seis.cum_moment)])

   
        # moment plot
        seis_maxes[0].set_zorder(1)
        seis_maxes[0].set_ylabel("Seismic moment\n [$10^{14}\,$Nm]", fontsize='large')
        seis_maxes[0].plot(Seis.index, Seis.cum_moment/moment_scale, color="black", label='Cumulative moment', zorder=4)
        seis_maxes[0].fill_between(Seis.index, 0 , Seis.cum_moment/moment_scale, facecolor=(.8,.8,.8), alpha=0.4)
        seis_maxes[0].set_ylim(*ylim_moment/moment_scale)

        seis_maxes[0].legend(loc=(0.05,0.79), numpoints=3, frameon=True, edgecolor="white", markerscale=1, framealpha=0.5)
        # stem plot
        seis_maxes[1].set_zorder(5)
        seis_maxes[1].set_ylabel('Magnitude', fontsize='large')

        markerline, stemlines, baseline = seis_maxes[1].stem(Seis.index, Seis.MLW,
                    use_line_collection=True)
        plt.setp(stemlines, ls="-", color=(.6,.6,.6), lw=0.4, alpha=1.0)
        plt.setp(markerline, mfc='blue', mec='blue', ms=6, label='Magnitude $\mathrm{M_{LW}}$')
        plt.setp(baseline, visible=False, alpha=0.1)
        #seis_maxes[0].set_xlim(xlim[0],xlim[1])
        seis_maxes[1].set_ylim(*ylim_magnitude)
        seis_maxes[1].legend(loc=(0.04,0.66), numpoints=1, frameon=True, edgecolor="white", markerscale=1, framealpha=0.5)
        area = [ "min_lon",  "max_lon", "min_lat",  "max_lat"]
    
        seis_infotext= sid + "Seismicity in the the area \n {:.2f} to {:.2f} and {:.2f} to {:.2f}".format(*[ constr[key] for key in  area ] ) 
        seis_axis.text(0.50, 0.80, seis_infotext, fontsize=15, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='center'  )

   
    
    
    
    def plotGPS():
        ## ploting GPS
        ylim_hlength=np.array([ylsc*min(GPS.hlength), ylsc*max(GPS.hlength)])


        gps_maxes[0].errorbar(GPS.index, GPS.hlength, yerr=GPS.Dhlength, ls='none', ecolor='grey', elinewidth=0.2, 
                         capsize=0.6, capthick=0.2)
        gps_maxes[0].set_ylabel("Displacement\n[mm]", fontsize='large', labelpad=4)
        gps_maxes[0].plot_date(GPS.index, GPS.hlength, marker='o', markersize=5, markerfacecolor='r', 
                   markeredgecolor='r', label='Horizontal displacemt')

        gps_maxes[0].errorbar(GPS.index, GPS.up, yerr=GPS.Dup, ls='none', ecolor='grey', elinewidth=0.2, 
                             capsize=0.6, capthick=0.2)
        #gps_maxes[1].set_ylabel("Vertical\ndisplacement [mm]", fontsize='large', labelpad=4)
        gps_maxes[0].plot_date(GPS.index, GPS.up, marker='o', markersize=5, markerfacecolor='b', 
                   markeredgecolor='b', label='Vertical displacemt')
        gps_maxes[0].legend(loc=(0.05,0.67), numpoints=1, frameon=True, edgecolor="white", markerscale=1, framealpha=0.5)

        gps_infotext="GPS station {}".format(sid)
        gps_axis.text(0.5, 0.9, gps_infotext, fontsize=15, transform=gps_axis.transAxes, 
                      bbox=dict(facecolor='orange', alpha=0.1), ha='center' )
        
        
    
        
        ## Hytrological data
    def plotHytro():
        
        TW1_color=(.6,.6,.6)
        TL1_color=(.8,.8,.8)
        hytro_maxes[2].set_zorder(1)
        hytro_maxes[2].patch.set_visible(False)
        hytro_maxes[2].set_ylabel("Temperature [$^{\circ}$C]", fontsize='large', labelpad=4)
        TW_plot = hytro_maxes[2].plot_date(Hytro.index, Hytro.Tw1_med_C, marker='o', markersize=1, markerfacecolor=TW1_color, 
                   markeredgecolor=TW1_color, label="Water Temperature")
        TL_plot = hytro_maxes[2].plot_date(Hytro.index, Hytro.TL1_med_C, marker='o', markersize=1, markerfacecolor=TL1_color, 
                   markeredgecolor=TL1_color, label="Air Temperature")


        hytro_maxes[0].set_zorder(10)
        hytro_maxes[0].patch.set_visible(False)
        hytro_maxes[0].set_ylabel("Conductivity\n[$\mu\mathrm{S\,cm^{-1}}$]", fontsize='large', labelpad=4)
        EC_plot = hytro_maxes[0].plot_date(Hytro.index, Hytro.EC_med_us, marker='o', markersize=1, markerfacecolor='r', 
                   markeredgecolor='r', label="Conductivity",  alpha=1.0)
        hytro_maxes[0].set_ylim(ymin=50) #CO2_max])

        hytro_maxes[1].set_zorder(3)
        hytro_maxes[1].patch.set_visible(False)
        hytro_maxes[1].set_ylabel("Water level [m]", fontsize='large', labelpad=4)
        Wl_plot=hytro_maxes[1].plot_date(Hytro.index, Hytro.W1_med_cm/100, marker='o', markersize=1, markerfacecolor='b', 
                   markeredgecolor='b', label="Water level", alpha=1.0)

    #hytro_axis.legend( (EC_plot), loc=(0.20,0.67), ncol=4, numpoints=1, frameon=True, edgecolor="white", markerscale=5, framealpha=0.5)
        hytro_maxes[2].legend(loc=(0.15,0.67), numpoints=1, frameon=True, edgecolor="white", markerscale=5, framealpha=0.5)
        hytro_maxes[0].legend(loc=(0.00,0.80), numpoints=1, frameon=True, edgecolor="white", markerscale=5, framealpha=0.5)
        hytro_maxes[1].legend(loc=(0.00,0.67), numpoints=1, frameon=True, edgecolor="white", markerscale=5, framealpha=0.5)


        hytro_infotext="Hytrological station in Mulakvísl {}".format(hytro_station)
        hytro_axis.text(0.50, 0.9, hytro_infotext, fontsize=15, transform=hytro_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='center' )

    
    
    ## Gas data
    def plotGas():
        
        gas_maxes[0].set_zorder(1)
        #CO2_max=GAS.co2_percentage1]*100+3
        gas_maxes[0].set_ylabel("Consentration [$\%$]", fontsize='large', labelpad=4)
        gas_infotext= station
        try:
            gas_maxes[0].plot_date(GAS.index, GAS.co2_percentage, marker='o', markersize=5, markerfacecolor='black', 
                    markeredgecolor='black', label="CO$_2$ [$\%$]", zorder=1)

            gas_maxes[1].set_zorder(1)
            maxgas = max(max(GAS["h2s_concentration"]) , max(GAS["so2_concentration"] ))
            #gas_maxes[1].set_ylim([-0.1,2.5*maxgas])
            gas_maxes[1].set_ylabel("Consentration\n[ppm]", fontsize='large', labelpad=4)
            gas_maxes[1].plot_date(GAS.index, GAS.h2s_concentration, marker='o', markersize=5, markerfacecolor='red', 
                       markeredgecolor='red', label='H$_2$S [ppm]',zorder=1)

            gas_maxes[1].plot_date(GAS.index, GAS.so2_concentration, marker='o', markersize=5, markerfacecolor='b', 
                      markeredgecolor='b', label='SO$_2$ [ppm]')
            gas_maxes[1].set_ylim(ymin=0, ymax=1.05*maxgas)
            #legend
            gas_maxes[1].legend(loc=(0.05,0.66), numpoints=1, frameon=True, edgecolor="white", framealpha=0.1)
            gas_maxes[0].legend(loc=(0.05,0.52), numpoints=1, frameon=True, edgecolor="white", framealpha=0.1)

            gas_axis.text(0.50, 0.9, gas_infotext, fontsize=15, transform=gas_axis.transAxes,
                          bbox=dict(facecolor='orange', alpha=0.1), ha='center' )
        except AttributeError:
            gas_axis.text(0.5, 0.5, gas_infotext+"\nNo Gas data found", fontsize=35, transform=gas_axis.transAxes,
                          bbox=dict(facecolor='orange', alpha=0.1), ha='center' )

        gas_maxes[0].set_ylim(ymin=0) #CO2_max])
    
    # write to image file
    home = Path.home()
    print(home)
    
    plotSeis() 
    plotGPS()
    plotHytro()
    plotGas()
    
    
    print("Testing new beta")
    relpath = "multiplot/figures/"
    filebase = "multiplot"
    filename = home.joinpath(relpath,filebase).as_posix()
    #filename = "/home/gpsops/multiplot/figures/multiplot"

    tplt.saveFig(filename, "png", fig)
    del fig


if __name__=="__main__":
    main(num1, num2, minlat, maxlat, minlon, maxlon, sid, station, hytro_station)

    
    
    
    
