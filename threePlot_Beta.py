#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function

import numpy as np
import geofunc.geo as geo
   
                    
def main(num1, num2,timeRange1,timeRange2,  minlat, maxlat, minlon, maxlon, sid):
    """
    """
    import cparser
    import logging

    from pathlib import Path
    from datetime import datetime as dt
    import gtimes.timefunc as tf
    #import timesmatplt.timesmatplt as tplt  # plot GPS data?
    import numpy as np # some simple calculations
    import geofunc.geo as geo
    import pandas as pd

    import geo_dataread.sil_read as gdrsil
    import matplotlib.dates as mdates  # better date/time axis ticks
    import timesmatplt.timesmatplt as tplt  # plot GPS data?

    # must register pandas datetime converter in matplotlib to avoid warning
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()
    
    
    from gtimes.timefunc import TimetoYearf
    
    import geo_dataread.gas_read as gdrgas
    import geo_dataread.sil_read as gdrsil
    import geo_dataread.hytro_read as gdrhyt
    import geo_dataread.gps_read as gdrgps
    
    import functions as ft
    
    dstr="%Y-%m-%d %H:%M:%S"
    dfstr="%Y-%m-%d"  # date format
    ### INPUT:
    #sid = "GFUM"
    start = tf.currDatetime(num1)
    start_date = dt.strftime(start, dstr)
    
    
    
    if num2 == 0:
        end_date=dt.strftime(dt.now(), dstr)
    else:
        end_date = tf.currDatetime(num2)
    
    
    #Búa til annað tímabil fyrir skjálftagögn
    start2 = tf.currDatetime(timeRange1)
    start_date2 = dt.strftime(start2, dstr)
    
    if timeRange2 == 0:
        end_date2=dt.strftime(dt.now(), dstr)
    else:
        end_date2 = tf.currDatetime(timeRange2)
     
        
    logging_level=logging.INFO
    ## Everything else from here is automatic:

    StaPars = cparser.Parser()

    dt_start = tf.toDatetime(start_date,dstr) # datetime obj
    dt_end = tf.toDatetime(end_date,dstr)
    f_start = tf.currYearfDate(refday=dt_start) # float (fractional year)
    f_end = tf.currYearfDate(refday=dt_end)
    

    dt_start2 = tf.toDatetime(start_date2,dstr) # datetime obj
    dt_end2 = tf.toDatetime(end_date2,dstr)
    f_start2 = tf.currYearfDate(refday=dt_start2) # float (fractional year)
    f_end2 = tf.currYearfDate(refday=dt_end2)
    
    
    
    #end = tf.currDatetime(-1500)

    pre_path= "/mnt_data/"
    
    ## Get GPS data with some basic filtering
    gps_path="gpsdata/"
    #-#detrend_period=[TimetoYearf(2001,1,1), TimetoYearf(2010,1,1)] # FOR HVOL
    #-#detrend_line=[TimetoYearf(2010,6,1), TimetoYearf(2014,4,1)] # FOR HVOL
    ## Get GPS data with some basic filtering
    detrend_period=[TimetoYearf(2001,1,1), TimetoYearf(2019,1,1)]
    detrend_line=[TimetoYearf(2010,6,1), TimetoYearf(2019,4,1)]

    #GPS = gdrgps.read_gps_data(sid, Dir=pre_path+gps_path, start=dt_start, end=dt_end, detrend_period=detrend_period, 
    #               detrend_line=detrend_line, logging_level=logging_level)
    GPS = gdrgps.read_gps_data(sid, Dir=pre_path+gps_path, start=dt_start, end=dt_end, detrend_period=None, 
                   detrend_line=None, logging_level=logging_level)

    
    ## Get seismicity data
    sil_path="sildata/"
    #filtering the seismisity
    
    
    constr = {  #"start": start,     "end": end, 
                "min_lat": minlat,  "max_lat":  maxlat,
                "min_lon": minlon, "max_lon": maxlon,
                #"min_depth": 15.0, "max_depth": None, 
                #"min_mlw": 1.0,  "max_mlw": None,
   }

    
    #constr = getConstr(min_lat, max_lat, min_lon, max_lon)
    
    
    
    col_names = ['latitude', 'Dlatitude', 'longitude', 'Dlongitude', 
                    'depth', 'Ddepth', 'MLW', 'seismic_moment', 'cum_moment']
    #Skjálftagögn
    Seis = gdrsil.read_sil_data(start=dt_start, end=dt_end, base_path=pre_path+sil_path, 
                            fread="events.fps",   **constr)
    
    
    #Búa til skjálftagögn fyrir annað tímabil
    Seis2 = gdrsil.read_sil_data(start=dt_start2, end=dt_end2, base_path=pre_path+sil_path, 
                            fread="events.fps",   **constr)
    
    
    loc = "IMO"
    if loc=="IMO":
        conn_dict={ 
               "dbname"    : "gas",
               "user"      : "gas_read",
               "password"  : "qwerty7",
               "host"      : "dev.psql.vedur.is",
               "port"      :  5432
               }
    
    elif loc=="home":
        # runn to make this work
        #ssh -f gpsops@rek.vedur.is -L 5431:dev.psql.vedur.is:5432 -N
        conn_dict={
                "dbname"    : "gas",
                "user"      : "gas_read",
                "password"  : "qwerty7",
                "host"      : "localhost",
                "port"      :  5431
                }
    
    
    ## Ploting
    import matplotlib.pyplot as plt

    xlsc=1.02 # x-axis limits w.r.t. data
    ylsc=1.05 # y-axis limits w.r.t. data (max.abs)
    moment_scale=1e14

    
    fig = ft.spltbbthreeFrame(Ylabel=None, Title=None)
    fig = ft.tsthreefigTickLabels(fig,period=(dt_end-dt_start),period2=(dt_end2-dt_start2))
    

    # setting upp the axis stuff
    
    
    seis_axis2 = fig.axes[0]
    seis_maxes2 = [seis_axis2, seis_axis2.twinx()]
    seis_axis2.set_xlim([dt_start2,dt_end2])
  
    seis_axis = fig.axes[1] 
    seis_maxes = [seis_axis, seis_axis.twinx()]
    seis_axis.set_xlim([dt_start,dt_end])
    
    gps_axis = fig.axes[2]
    gps_maxes = [gps_axis]
    gps_axis.set_xlim([dt_start,dt_end])
    fig.subplots_adjust(right=1)
    
    
    
    
    def plotSeisDifRange():
        
        ## ploting seismic
        ylim_magnitude=np.array([0, ylsc*max(Seis2.MLW)])
        ylim_moment=np.array([0, ylsc*max(Seis2.cum_moment)])

        # moment plot
        seis_maxes2[0].set_zorder(1)
        seis_maxes2[0].set_ylabel("Seismic moment\n [$10^{14}\,$Nm]", fontsize='large')
        seis_maxes2[0].plot(Seis2.index, Seis2.cum_moment/moment_scale, color="black", label='Cumulative moment', zorder=4)
        seis_maxes2[0].fill_between(Seis2.index, 0 , Seis2.cum_moment/moment_scale, facecolor=(.8,.8,.8), alpha=0.4)
        seis_maxes2[0].set_ylim(*ylim_moment/moment_scale)

        seis_maxes2[0].legend(loc=(0.05,0.79), numpoints=3, frameon=True, edgecolor="white", markerscale=1, framealpha=0.5)
        # stem plot
        seis_maxes2[1].set_zorder(5)
        seis_maxes2[1].set_ylabel('Magnitude', fontsize='large')

        markerline, stemlines, baseline = seis_maxes2[1].stem(Seis2.index, Seis2.MLW,
                    use_line_collection=True)
        plt.setp(stemlines, ls="-", color=(.6,.6,.6), lw=0.4, alpha=1.0)
        plt.setp(markerline, mfc='blue', mec='blue', ms=6, label='Magnitude $\mathrm{M_{LW}}$')
        plt.setp(baseline, visible=False, alpha=0.1)
        #seis_maxes[0].set_xlim(xlim[0],xlim[1])
        seis_maxes2[1].set_ylim(*ylim_magnitude)
        seis_maxes2[1].legend(loc=(0.04,0.66), numpoints=1, frameon=True, edgecolor="white", markerscale=1, framealpha=0.5)
        area = [ "min_lon",  "max_lon", "min_lat",  "max_lat"]
    
        seis_infotext= sid + "Seismicity in the the area \n {:.2f} to {:.2f} and {:.2f} to {:.2f}".format(*[ constr[key] for key in  area ] ) 
        seis_axis2.text(0.50, 0.80, seis_infotext, fontsize=15, transform=seis_axis2.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='center'  )


    def plotSeis():
        
        ## ploting seismic
        ylim_magnitude=np.array([0, ylsc*max(Seis.MLW)])
        ylim_moment=np.array([0, ylsc*max(Seis.cum_moment)])

        # moment plot
        seis_maxes[0].set_zorder(1)
        seis_maxes[0].set_ylabel("Seismic moment\n [$10^{14}\,$Nm]", fontsize='large')
        seis_maxes[0].plot(Seis.index, Seis.cum_moment/moment_scale, color="black", label='Cumulative moment', zorder=4)
        seis_maxes[0].fill_between(Seis.index, 0 , Seis.cum_moment/moment_scale, facecolor=(.8,.8,.8), alpha=0.4)
        seis_maxes[0].set_ylim(*ylim_moment/moment_scale)

        seis_maxes[0].legend(loc=(0.05,0.79), numpoints=3, frameon=True, edgecolor="white", markerscale=1, framealpha=0.5)
        # stem plot
        seis_maxes[1].set_zorder(5)
        seis_maxes[1].set_ylabel('Magnitude', fontsize='large')

        markerline, stemlines, baseline = seis_maxes[1].stem(Seis.index, Seis.MLW,
                    use_line_collection=True)
        plt.setp(stemlines, ls="-", color=(.6,.6,.6), lw=0.4, alpha=1.0)
        plt.setp(markerline, mfc='blue', mec='blue', ms=6, label='Magnitude $\mathrm{M_{LW}}$')
        plt.setp(baseline, visible=False, alpha=0.1)
        #seis_maxes[0].set_xlim(xlim[0],xlim[1])
        seis_maxes[1].set_ylim(*ylim_magnitude)
        seis_maxes[1].legend(loc=(0.04,0.66), numpoints=1, frameon=True, edgecolor="white", markerscale=1, framealpha=0.5)
        area = [ "min_lon",  "max_lon", "min_lat",  "max_lat"]
    
        seis_infotext= sid + "Seismicity in the the area \n {:.2f} to {:.2f} and {:.2f} to {:.2f}".format(*[ constr[key] for key in  area ] ) 
        seis_axis.text(0.50, 0.80, seis_infotext, fontsize=15, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='center'  )

   
    
    
    
    def plotGPS():
        ## ploting GPS
        ylim_hlength=np.array([ylsc*min(GPS.hlength), ylsc*max(GPS.hlength)])


        gps_maxes[0].errorbar(GPS.index, GPS.hlength, yerr=GPS.Dhlength, ls='none', ecolor='grey', elinewidth=0.2, 
                         capsize=0.6, capthick=0.2)
        gps_maxes[0].set_ylabel("Displacement\n[mm]", fontsize='large', labelpad=4)
        gps_maxes[0].plot_date(GPS.index, GPS.hlength, marker='o', markersize=5, markerfacecolor='r', 
                   markeredgecolor='r', label='Horizontal displacemt')

        gps_maxes[0].errorbar(GPS.index, GPS.up, yerr=GPS.Dup, ls='none', ecolor='grey', elinewidth=0.2, 
                             capsize=0.6, capthick=0.2)
        #gps_maxes[1].set_ylabel("Vertical\ndisplacement [mm]", fontsize='large', labelpad=4)
        gps_maxes[0].plot_date(GPS.index, GPS.up, marker='o', markersize=5, markerfacecolor='b', 
                   markeredgecolor='b', label='Vertical displacemt')
        gps_maxes[0].legend(loc=(0.05,0.67), numpoints=1, frameon=True, edgecolor="white", markerscale=1, framealpha=0.5)

        gps_infotext="GPS station {}".format(sid)
        gps_axis.text(0.5, 0.9, gps_infotext, fontsize=15, transform=gps_axis.transAxes, 
                      bbox=dict(facecolor='orange', alpha=0.1), ha='center' )
        
        
    

 
    # write to image file
    home = Path.home()
    print(home)
    
    plotSeisDifRange()
    plotSeis()
    plotGPS()
    
    
  
    print("Testing new beta")
    relpath = "multiplot/figures/"
    filebase = "threePlot"
    filename = home.joinpath(relpath,filebase).as_posix()
    #filename = "/home/gpsops/multiplot/figures/multiplot"

    tplt.saveFig(filename, "png", fig)
    del fig


if __name__=="__main__":
    main(num1, num2, timeRange1,timeRange2, minlat, maxlat, minlon, maxlon, sid)

    
    