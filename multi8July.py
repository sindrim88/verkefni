#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function


from pylab import *
import numpy as np
import geofunc.geo as geo
import datetime

from datetime import datetime
from datetime import datetime


from matplotlib.colors import LinearSegmentedColormap

from matplotlib import cm
import matplotlib
matplotlib.pyplot.stem
matplotlib.axes.Axes.stem

def main(num1, num2,timeRange1,timeRange2):
    """
    """
    import cparser
    import logging

    from pathlib import Path
    from datetime import datetime as dt
    import gtimes.timefunc as tf
    #import timesmatplt.timesmatplt as tplt  # plot GPS data?
    import numpy as np # some simple calculations
    import geofunc.geo as geo
    import pandas as pd

    import geo_dataread.sil_read as gdrsil
    import matplotlib.dates as mdates  # better date/time axis ticks
    import timesmatplt.timesmatplt as tplt  # plot GPS data?

    # must register pandas datetime converter in matplotlib to avoid warning
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()
    
    import matplotlib.patches as mpatches
    from gtimes.timefunc import TimetoYearf
    
    import geo_dataread.gas_read as gdrgas
    import geo_dataread.sil_read as gdrsil
    import geo_dataread.hytro_read as gdrhyt
    import geo_dataread.gps_read as gdrgps
    import matplotlib.lines as mlines
    import functions as ft
    from datetime import timedelta
    
    now = datetime.now()
    
    dstr="%Y-%m-%d %H:%M:%S"
    dfstr="%Y-%m-%d"  # date format
    ### INPUT:
    sid = "GJOG"
    start = tf.currDatetime(num1)
    start_date = dt.strftime(start, dstr)

    if num2 == 0:
        end_date=dt.strftime(dt.now(), dstr)
    else:
        end_date = tf.currDatetime(num2)
    
    #Búa til annað tímabil fyrir skjálftagögn
    start2 = tf.currDatetime(timeRange1)
    start_date2 = dt.strftime(start2, dstr)
    
    if timeRange2 == 0:
        end_date2=dt.strftime(dt.now(), dstr)
    else:
        end_date2 = tf.currDatetime(timeRange2)
     
    logging_level=logging.INFO
    ## Everything else from here is automatic:

    StaPars = cparser.Parser()

    dt_start = tf.toDatetime(start_date,dstr) # datetime obj
    dt_end = tf.toDatetime(end_date,dstr)
    f_start = tf.currYearfDate(refday=dt_start) # float (fractional year)
    f_end = tf.currYearfDate(refday=dt_end)
    
    dt_start2 = tf.toDatetime(start_date2,dstr) # datetime obj
    dt_end2 = tf.toDatetime(end_date2,dstr)
    f_start2 = tf.currYearfDate(refday=dt_start2) # float (fractional year)
    f_end2 = tf.currYearfDate(refday=dt_end2)
   
    timestamp_format="%a %-d.%b %Y, %H:%M"
    timestamp=dt.now()
  
    pend = timestamp + (dt_end-dt_start)/40
    pend2 = timestamp + (dt_end2-dt_start2)/40
    
    if (timestamp - dt_end2) > timedelta(minutes=15):
        color_warn="r"
    else:
        color_warn="g"
    
    pre_path= "/mnt_data/"
    
    ## Get GPS data with some basic filtering
    gps_path="gpsdata/"
    #-#detrend_period=[TimetoYearf(2001,1,1), TimetoYearf(2010,1,1)] # FOR HVOL
    #-#detrend_line=[TimetoYearf(2010,6,1), TimetoYearf(2014,4,1)] # FOR HVOL
    ## Get GPS data with some basic filtering
    detrend_period=[TimetoYearf(2001,1,1), TimetoYearf(2019,1,1)]
    detrend_line=[TimetoYearf(2010,6,1), TimetoYearf(2019,4,1)]

    #GPS = gdrgps.read_gps_data(sid, Dir=pre_path+gps_path, start=dt_start, end=dt_end, detrend_period=detrend_period, 
    #               detrend_line=detrend_line, logging_level=logging_level)
    GPS = gdrgps.read_gps_data(sid, Dir=pre_path+gps_path, start=dt_start, end=dt_end, detrend_period=None, 
                   detrend_line=None, logging_level=logging_level)

    ## Get seismicity data
    sil_path="sildata/"
    #filtering the seismisity
    
    constr = {  #"start": start,     "end": end, 
                "min_lat": 66.00,  "max_lat":  66.6,
                "min_lon": -18.8, "max_lon": -17.2,
                "min_Q": 30.0, "max_Q": None,  
                #"min_depth": 15.0, "max_depth": None, 
                #"min_mlw": 1.0,  "max_mlw": None,
   }
    
    col_names = ['latitude', 'Dlatitude', 'longitude', 'Dlongitude', 
                    'depth', 'Ddepth', 'MLW', 'seismic_moment', 'cum_moment', 'Q']
    
    Seis = Seis2 = gdrsil.read_sil_data(start=dt_start, end=dt_end, base_path=pre_path+sil_path,  
            fread="default", aut=True, aut_per=2, rcolumns=col_names, cum_moment=True, logger='default', **constr)

   
    loc = "IMO"
    if loc=="IMO":
        conn_dict={ 
               "dbname"    : "gas",
               "user"      : "gas_read",
               "password"  : "qwerty7",
               "host"      : "dev.psql.vedur.is",
               "port"      :  5432
               }
    
    elif loc=="home":
        # runn to make this work
        #ssh -f gpsops@rek.vedur.is -L 5431:dev.psql.vedur.is:5432 -N
        conn_dict={
                "dbname"    : "gas",
                "user"      : "gas_read",
                "password"  : "qwerty7",
                "host"      : "localhost",
                "port"      :  5431
                }
    
    ## Ploting
    import matplotlib.pyplot as plt

    xlsc=1.02 # x-axis limits w.r.t. data
    ylsc=1.05 # y-axis limits w.r.t. data (max.abs)
    moment_scale=1e14
 
    Title="Graf gert: {}".format( timestamp.strftime(timestamp_format) )
    str3 = "{}".format( timestamp.strftime(timestamp_format) )
    print(timestamp.strftime(timestamp_format) ) #testing
    print(str3) #testing

    depth = True
    fig = ft.fourFrame(Ylabel=None, Title=Title, depth = depth)
    fig = ft.tstwofigTickLabelsFourFrame(fig,period=(dt_end-dt_start),period2=(dt_end2-dt_start2))
    
    supTitle="Jardskjalftamaelingar vid Tjornes"
    fig.suptitle(supTitle, fontsize=40, verticalalignment="center",x = 0.813,y = 0.935)
    
    # setting upp the axis stuf  
    seis_axis = fig.axes[0]
    seis_maxes = [seis_axis, seis_axis.twinx()]
    seis_axis.set_xlim([dt_start2,dt_end2])
    seis_maxes[0].set_title(Title, color=color_warn, verticalalignment="bottom")
    
    seis_axisLat = fig.axes[1]
    seis_maxesLat = [seis_axisLat, seis_axisLat.twinx()]
    seis_axisLat.set_xlim([dt_start2,dt_end2])
    
    seis_axisDepth = fig.axes[2]
    seis_maxesDepth = [seis_axisDepth, seis_axisDepth.twinx()]
    seis_axisDepth.set_xlim([dt_start2,dt_end2])
    
    gps_axis = fig.axes[3]
    gps_maxes = [gps_axis, gps_axis.twinx(), gps_axis.twinx(),gps_axis.twinx()] 
    gps_axis.set_xlim([dt_start,dt_end])

    gps_maxes[2].spines['right'].set_position(("axes", 1.2))
    gps_maxes[2].set_frame_on(True)
    gps_maxes[2].patch.set_visible(True)
  
    fig.subplots_adjust(right=1.5)
    
    #--------------------
    Seis['Mcolors'] = float('nan')
   
    Seis.loc[  Seis['MLW'] < 2 , 'Mcolors'] = 'green'
    Seis.loc[ ( Seis['MLW'] >= 2 ) & ( Seis['MLW'] < 3 ) , 'Mcolors'] = 'gold'
    Seis.loc[ ( Seis['MLW'] >= 3 ) & ( Seis['MLW'] < 4 ) , 'Mcolors'] = 'darkorange'
    Seis.loc[ ( Seis['MLW'] >= 4 ) & ( Seis['MLW'] < 5 ) , 'Mcolors'] = 'orangered'
    Seis.loc[ ( Seis['MLW'] >= 5 ) & ( Seis['MLW'] < 6 ) , 'Mcolors'] = 'red'
    Seis.loc[ ( Seis['MLW'] >= 6 ), 'Mcolors'] = 'darkred'
    #----------------------------------------------
    
    Seis['Medge'] = float('nan')
    Seis.loc[ Seis["Q"] > 95, 'Medge' ] = 'black'
    Seis.loc[ Seis["Q"] <= 95, 'Medge' ] = 'face'
    
    Q = Seis[ Seis["Q"] > 95]
    Y = Seis[ Seis["Q"] <= 95]
    
    seis_maxes[0].set_zorder(0) 
    ylim_magnitude=np.array([0, ylsc*max(Seis.MLW)])
    ylim_moment=np.array([0, ylsc*max(Seis.cum_moment)])
    
    
    #Kennileiti ...  getur verið krefjandi að gera sjálfvirkt
    seis_axis.text(0.01, 0.85, "Husavik", fontsize= 23, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
    seis_axis.text(0.01, 0.420, "Gjogurta", fontsize= 23, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
    seis_axis.text(0.01, 0.16, "Eyjafjardarall", fontsize= 23, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
    seis_axis.text(0.01, 0.62, "Flatey", fontsize= 23, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
    
    
    seis_axisLat.text(0.01, -0.98, "Husavik", fontsize= 23, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
    seis_axisLat.text(0.0051, -0.8, "Gjogurta", fontsize= 23, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
    seis_axisLat.text(0.01, -0.22, "Eyjafjardarall", fontsize= 23, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
    



    #markSize = [22.5, 20, 17.5, 15, 12.5, 10] 
    patchColors = ["darkred", "red","orangered","darkorange","gold","green"]
    texts = ["6+", "5-6", "4-5", "3-4", "2-3", "0-2"]
    
    patches = [ plt.plot([],[], marker="o", ms=25,  mec='black', color=patchColors[i], 
            label="{:s}".format(texts[i]))[0]  for i in range(len(texts)) ]
    
    seis_maxes[0].legend(facecolor=None, framealpha=0, handles=patches,bbox_to_anchor=(0.15, 0.87), 
           loc='center', ncol=2, numpoints=1 ,fontsize = 20)
    
    
    
    seis_maxes[0].set_xlim([dt_start2, pend2])
    seis_maxes[0].set_ylabel("Longitude", fontsize=20, labelpad=4)
    seis_maxes[0].set_ylim(-19,-17)

    ##testing times
    timestamp2 = Y.index.max()
    str3 = timestamp2.strftime(timestamp_format)
    
    end = Y.index.max()
    temp = str(end)
 
    print("Str3")
    print(timestamp2)#testing
    
    #Longitude and Latitude horizontal lines
    seis_maxes[0].hlines(y= -17.34, xmin=Q.index.min(), xmax=temp, color='gray', zorder=1)
    seis_maxes[0].hlines(y= -17.81, xmin=Q.index.min(), xmax=temp, color='gray', zorder=1)
    seis_maxes[0].hlines(y= -18.72, xmin=Q.index.min(), xmax=temp, color='gray', zorder=1)
    seis_maxes[0].hlines(y= -18.20, xmin=Q.index.min(), xmax=temp, color='gray', zorder=1)
        
    seis_maxesLat[1].hlines(y= 66.047, xmin=Q.index.min(), xmax=temp, color='gray', zorder=1)
    seis_maxesLat[1].hlines(y= 66.173, xmin=Q.index.min(), xmax=temp, color='gray', zorder=1)
    seis_maxesLat[1].hlines(y= 66.47, xmin=Q.index.min(), xmax=temp, color='gray', zorder=1)
    
    
    #Time update vertical lines
    seis_maxes[0].axvline(temp, color='gray')
    seis_maxesLat[0].axvline(temp, color='gray')
    seis_maxesDepth[0].axvline(temp, color='gray')
    gps_maxes[0].axvline(temp, color='gray')
   
    ####
    ####   Scatter Longitude earthquakes
    ####
    
    seis_maxes[0].scatter(Y.index, Y["longitude"], alpha = 0.75, c=Y['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Y['MLW'], marker = 'o',edgecolors='face')
    
    seis_maxes[0].scatter(Q.index, Q["longitude"], alpha = 0.75, c=Q['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Q['MLW'], marker = 'o',edgecolors='black')
    
    area = [ "min_lon",  "max_lon", "min_lat",  "max_lat"]
    seis_infotext="Seismicity in the GJOG area\n {:.2f} to {:.2f} and {:.2f} to {:.2f}".format(*[ constr[key] for key in  area ] ) 
    seis_axis.text(0.50, 0.90, seis_infotext, fontsize=15, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='center'  )


    ####   Scatter Latitude earthquakes...

    seis_maxesLat[0].set_xlim([dt_start2, pend2])
    seis_maxesLat[0].set_ylabel("Latitude", fontsize=20, labelpad=4)
 
    #Scatter Latitude earthquakes
    seis_maxesLat[0].scatter(Y.index, Y["latitude"], alpha = 0.75, c=Y['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Y['MLW'], marker = 'o',edgecolors='face')
    
    seis_maxesLat[0].scatter(Q.index, Q["latitude"], alpha = 0.75, c=Q['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Q['MLW'], marker = 'o',edgecolors='black')


    seis_maxesDepth[0].set_xlim([dt_start2, pend2])
    seis_maxesDepth[0].set_zorder(1)
    seis_maxesDepth[0].patch.set_visible(False)

    
    ####   Scatter Longitude earthquakes..

    seis_maxesDepth[1].scatter(Y.index, Y["depth"], alpha = 0.75, c=Y['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Y['MLW'], marker = 'o',edgecolors='face')
    
    seis_maxesDepth[1].scatter(Q.index, Q["depth"], alpha = 0.75, c=Q['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Q['MLW'], marker = 'o',edgecolors='black')


    #### Here I use  seis_maxesDepth to plot the time update on Lat and Lon plots, should be fine as it's the same data.
    #### For some reason seis_maxesLat[1] and seis_maxes[1] give errors
 
    seis_maxesDepth[1].annotate(timestamp2, xy=(end+timedelta(0), 0), xytext=(10,250), textcoords='offset points', horizontalalignment='left', fontsize=20, rotation="vertical", color=color_warn)
    
    seis_maxesDepth[1].annotate(timestamp2, xy=(end+timedelta(0), 0), xytext=(10,700), textcoords='offset points', horizontalalignment='left', fontsize=20, rotation="vertical", color=color_warn)
    
    seis_maxesDepth[0].set_ylabel("Depth [km]", fontsize=20, labelpad=4)

    
    ####   Plot gps stuff
    
    gps_maxes[0].set_zorder(1)
    gps_maxes[0].patch.set_visible(False)
    gps_maxes[0].set_xlim([start, pend])
    gps_maxes[0].errorbar(GPS.index, GPS.hlength, yerr=GPS.Dhlength, ls='none', ecolor='grey', elinewidth=0.2, 
                     capsize=0.6, capthick=0.2)
    gps_maxes[0].set_ylabel("Displacement\n[mm]", fontsize=20, labelpad=4)
    gps_maxes[0].plot_date(GPS.index, GPS.hlength, marker='o', markersize=8, markerfacecolor='r', 
               markeredgecolor='r', label='Horizontal displacemt')

    gps_maxes[0].errorbar(GPS.index, GPS.up, yerr=GPS.Dup, ls='none', ecolor='grey', elinewidth=0.2, 
                         capsize=0.6, capthick=0.2)
    #gps_maxes[1].set_ylabel("Vertical\ndisplacement [mm]", fontsize='large', labelpad=4)
    gps_maxes[0].plot_date(GPS.index, GPS.up, marker='o', markersize=8, markerfacecolor='black', 
               markeredgecolor='black', label='Vertical displacemt')
    gps_maxes[0].legend(loc=(0.05,0.67), numpoints=1, frameon=True, edgecolor="white", markerscale=2, framealpha=0.5)

    gps_infotext="GPS station {}".format(sid)
    gps_axis.text(0.5, 0.9, gps_infotext, fontsize=15, transform=gps_axis.transAxes, 
                  bbox=dict(facecolor='orange', alpha=0.1), ha='center' )
    
    
    gps_maxes[1].annotate(timestamp2, xy=(end+timedelta(0), 0), xytext=(10,50), textcoords='offset points', horizontalalignment='left', fontsize=20, rotation="vertical", color=color_warn)
    
 
    ####   Plot seismic moment

    gps_maxes[1].set_zorder(3)
    gps_maxes[1].patch.set_visible(False)
    gps_maxes[1].set_xlim([start, pend])
    gps_maxes[1].set_ylabel("Seismic moment\n [$10^{14}\,$Nm]", fontsize=24, labelpad=4)
    
    gps_maxes[1].plot(Seis.index, Seis.cum_moment/moment_scale, color="black", zorder=4 , label= 'asdasdad')
    gps_maxes[1].fill_between(Seis.index, 0, Seis.cum_moment/moment_scale, facecolor=(.8,.8,.8), alpha=0.4, label = 'tsdast')
    
        
     
    ####   Plot colorcoded earthquakes
    
    #handles the color coded earthquakes by date
    cmap = matplotlib.colors.LinearSegmentedColormap.from_list("",["blue","orange","red"])
    
    #create a smaller datafram for earthquakes bigger tan 4.5
    theBigOnes = Seis.loc[Seis['MLW'] >= 4.5]
    print(theBigOnes) # testing

    texts = ["Moment magnitude [$\mathrm{M_{W}}$] for reviewed events) / Local magnitude [$\mathrm{M_{L}}$]  for not reviewed events"]
    
    patches = [ plt.plot([],[], marker="o", ms=15,  mec='black', color='gray', 
            label="{:s}".format(texts[i]))[0]  for i in range(len(texts)) ]
   
    gps_maxes[2].legend(facecolor=None, framealpha=0, handles=patches,bbox_to_anchor=(0.215, 0.90), 
           loc='center', ncol=2, numpoints=1 ,fontsize = 20)
    
    gps_maxes[2].patch.set_visible(False)
    gps_maxes[2].set_zorder(5)
    gps_maxes[2].set_xlim([start, pend])
    
    #Plot colorcoded earthquakes by date
    gps_maxes[2].scatter(Seis.index, Seis.MLW, c=Seis.index, cmap=cmap, s = 100,  marker =  "o", linewidth = 0.3, edgecolors='k')
    
    #   Bigger earthquakes are drawn twice to
    #   the lime green stars, inefficient, but they are not many.
    gps_maxes[2].scatter(theBigOnes.index, theBigOnes.MLW, c= 'lime', cmap=cmap, s = 650,  marker =  "*", linewidth = 0.4, edgecolors='k')
        
    
    ### I put the stems on another y axis, so the stems are the fourth y-axis and are draw before the "*" quakes.
    gps_maxes[3].set_zorder(1)
    gps_maxes[3].set_xlim([start, pend])
    
    gps_maxes[3].stem(Seis.index,  Seis.MLW, 'gray', markerfmt=" ", use_line_collection = True)
    gps_maxes[3].set_ylabel("Magnitude [$\mathrm{M_{W}}$/$\mathrm{M_{L}}$]", fontsize=24, labelpad=4,)

    #Set axis limit
    seis_maxes[0].set_ylim(-19,-17)
    #seis_maxes[1].set_ylim(-19,-17)
    seis_maxes[1].set_yticks([])
    
    seis_maxesLat[0].set_ylim(65.9,66.8)
    #seis_maxesLat[1].set_ylim(65.9,66.8)
    seis_maxesLat[1].set_yticks([])
    
    seis_maxesDepth[0].set_ylim(20, 0)
    #seis_maxesDepth[1].set_ylim(20, 0)
    seis_maxesDepth[1].set_yticks([])
    
    gps_maxes[-1].spines['right'].set_position(('axes', 1.05))
    gps_maxes[2].spines['right'].set_position(('axes', 1.05))
    
    gps_maxes[0].set_ylim(ymin=-30, ymax = 30)
    gps_maxes[1].set_ylim(*ylim_moment/moment_scale*1.5)
    gps_maxes[2].set_ylim(ymin = 0, ymax = 11)
    gps_maxes[3].set_ylim(ymin = 0, ymax = 11)
    
    
    print("done")
                                 
    # write to image file
    home = Path.home()
    print(home)

    
    print("Testing new beta")
    relpath = "multiplot/figures/"
    filebase = "GjogPlot4"
    filename = home.joinpath(relpath,filebase).as_posix()
    #filename = "/home/gpsops/multiplot/figures/multiplot"
    
    tplt.saveFig(filename, "png", fig)
    #tplt.saveFig(filename, "pdf", fig)
    
    del fig

    
if __name__=="__main__":
    main(-60, 0, -3, 0)
