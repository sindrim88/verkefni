#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function


from pylab import *
import numpy as np
import geofunc.geo as geo
import datetime

from datetime import datetime
from datetime import datetime


from matplotlib.colors import LinearSegmentedColormap

from matplotlib import cm
import matplotlib
matplotlib.pyplot.stem
matplotlib.axes.Axes.stem



####   Scatter Longitude earthquakes
def plotLon(Q2,Y2, seis_maxes, constr, seis_axis, dt_start2, pend2):
    print(Q2.index.max)
    print(Y2.index.max)
    print()
    #Set axis limit
    seis_maxes[0].set_zorder(0) 
    seis_maxes[0].set_ylim(-19,-17)
    seis_maxes[1].set_yticks([])
    
    seis_maxes[0].set_xlim([dt_start2, pend2])
    seis_maxes[0].set_ylabel("Longitude", fontsize=20, labelpad=4)
    seis_maxes[0].set_ylim(-19,-17)
    
    seis_maxes[0].scatter(Y2.index, Y2["longitude"], alpha = 0.75, c=Y2['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Y2['MLW'], marker = 'o',edgecolors='face')
    
    seis_maxes[0].scatter(Q2.index, Q2["longitude"], alpha = 0.75, c=Q2['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Q2['MLW'], marker = 'o',edgecolors='black')
    
    area = [ "min_lon",  "max_lon", "min_lat",  "max_lat"]
    seis_infotext="Seismicity in the GJOG area\n {:.2f} to {:.2f} and {:.2f} to {:.2f}".format(*[ constr[key] for key in  area ] ) 
    seis_axis.text(0.50, 0.90, seis_infotext, fontsize=15, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='center'  )

    return
    


####   Scatter Latitude earthquakes...
def plotLat(Q2,Y2, seis_maxesLat, dt_start2, pend2):
    ####   Scatter Latitude earthquakes...

    seis_maxesLat[0].set_xlim([dt_start2, pend2])
    
    print("Testing Pend2")
    print(pend2)
    seis_maxesLat[0].set_ylabel("Latitude", fontsize=20, labelpad=4)
 
    #Scatter Latitude earthquakes
    seis_maxesLat[0].scatter(Y2.index, Y2["latitude"], alpha = 0.75, c=Y2['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Y2['MLW'], marker = 'o',edgecolors='face')
    
    seis_maxesLat[0].scatter(Q2.index, Q2["latitude"], alpha = 0.75, c=Q2['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Q2['MLW'], marker = 'o',edgecolors='black')

    
    seis_maxesLat[0].set_ylim(65.9,66.8)
    seis_maxesLat[1].set_yticks([])
    
    return



def plotDepth(Q2,Y2, seis_maxesDepth, dt_start2, pend2):
    
    seis_maxesDepth[0].set_xlim([dt_start2, pend2])
    seis_maxesDepth[0].set_zorder(1)
    seis_maxesDepth[0].patch.set_visible(False)

    seis_maxesDepth[1].scatter(Y2.index, Y2["depth"], alpha = 0.75, c=Y2['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Y2['MLW'], marker = 'o',edgecolors='face')
    
    seis_maxesDepth[1].scatter(Q2.index, Q2["depth"], alpha = 0.75, c=Q2['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Q2['MLW'], marker = 'o',edgecolors='black')
    
    seis_maxesDepth[0].set_ylim(20, 0)
    seis_maxesDepth[1].set_yticks([])

    return



def plotGps(GPS,gps_maxes,gps_axis, start, pend, sid):
    
    gps_maxes[0].set_zorder(1)
    gps_maxes[0].patch.set_visible(False)
    gps_maxes[0].set_xlim([start, pend])
    gps_maxes[0].errorbar(GPS.index, GPS.hlength, yerr=GPS.Dhlength, ls='none', ecolor='grey', elinewidth=0.2, 
                     capsize=0.6, capthick=0.2)
    gps_maxes[0].set_ylabel("Displacement\n[mm]", fontsize=20, labelpad=4)
    gps_maxes[0].plot_date(GPS.index, GPS.hlength, marker='o', markersize=8, markerfacecolor='r', 
               markeredgecolor='r', label='Horizontal displacemt')

    gps_maxes[0].errorbar(GPS.index, GPS.up, yerr=GPS.Dup, ls='none', ecolor='grey', elinewidth=0.2, 
                         capsize=0.6, capthick=0.2)
    #gps_maxes[1].set_ylabel("Vertical\ndisplacement [mm]", fontsize='large', labelpad=4)
    gps_maxes[0].plot_date(GPS.index, GPS.up, marker='o', markersize=8, markerfacecolor='black', 
               markeredgecolor='black', label='Vertical displacemt')
    gps_maxes[0].legend(loc=(0.05,0.67), numpoints=1, frameon=True, edgecolor="white", markerscale=2, framealpha=0.5)

    gps_infotext="GPS station {}".format(sid)
    gps_axis.text(0.5, 0.9, gps_infotext, fontsize=15, transform=gps_axis.transAxes, 
                  bbox=dict(facecolor='orange', alpha=0.1), ha='center' )
    
    return



#this plot is drawn on top of the gps plot:    plotGps(GPS,gps_maxes,gps_axis, start, pend, sid):
def plotMomentAndcolorCodeQukes(Seis, start, pend,gps_maxes):
    
    xlsc=1.02 # x-axis limits w.r.t. data
    ylsc=1.05 # y-axis limits w.r.t. data (max.abs)
    moment_scale=1e14
    ylim_magnitude=np.array([0, ylsc*max(Seis.MLW)])
    ylim_moment=np.array([0, ylsc*max(Seis.cum_moment)])
    
    gps_maxes[1].plot(Seis.index, Seis.cum_moment/moment_scale, color="black", zorder=4 , label= 'asdasdad')
    gps_maxes[1].fill_between(Seis.index, 0, Seis.cum_moment/moment_scale, facecolor=(.8,.8,.8), alpha=0.4, label = 'tsdast')
    
    cmap = matplotlib.colors.LinearSegmentedColormap.from_list("",["blue","orange","red"])
    
    #create a smaller dataframe for earthquakes bigger tan 4.5
    theBigOnes = Seis.loc[Seis['MLW'] >= 4.5]
    print(theBigOnes) # testing

    texts = ["Moment magnitude [$\mathrm{M_{W}}$] for reviewed events) / Local magnitude [$\mathrm{M_{L}}$]  for not reviewed events"]
    
    patches = [ plt.plot([],[], marker="o", ms=15,  mec='black', color='gray', 
            label="{:s}".format(texts[i]))[0]  for i in range(len(texts)) ]
   
    gps_maxes[2].legend(facecolor=None, framealpha=0, handles=patches,bbox_to_anchor=(0.215, 0.90), 
           loc='center', ncol=2, numpoints=1 ,fontsize = 20)
    
    gps_maxes[2].patch.set_visible(False)
    gps_maxes[2].set_zorder(5)
    gps_maxes[2].set_xlim([start, pend])
    
    #Plot colorcoded earthquakes by date
    gps_maxes[2].scatter(Seis.index, Seis.MLW, c=Seis.index, cmap=cmap, s = 100,  marker =  "o", linewidth = 0.3, edgecolors='k')
    
    #   Bigger earthquakes are drawn twice to
    #   the lime green stars, inefficient, but they are not many.
    gps_maxes[2].scatter(theBigOnes.index, theBigOnes.MLW, c= 'lime', cmap=cmap, s = 650,  marker =  "*", linewidth = 0.4, edgecolors='k')
    
    gps_maxes[-1].spines['right'].set_position(('axes', 1.05))
    gps_maxes[2].spines['right'].set_position(('axes', 1.05))
    
    gps_maxes[1].set_zorder(3)
    gps_maxes[1].patch.set_visible(False)
    gps_maxes[1].set_xlim([start, pend])
    gps_maxes[1].set_ylabel("Seismic moment\n [$10^{14}\,$Nm]", fontsize=24, labelpad=4)
    
    gps_maxes[3].set_zorder(1)
    gps_maxes[3].set_xlim([start, pend])
    gps_maxes[3].stem(Seis.index,  Seis.MLW, 'gray', markerfmt=" ", use_line_collection = True)
    gps_maxes[3].set_ylabel("Magnitude [$\mathrm{M_{W}}$/$\mathrm{M_{L}}$]", fontsize=24, labelpad=4,)
    
    gps_maxes[0].set_ylim(ymin=-30, ymax = 30)
    gps_maxes[1].set_ylim(*ylim_moment/moment_scale*1.5)
    gps_maxes[2].set_ylim(ymin = 0, ymax = 11)
    gps_maxes[3].set_ylim(ymin = 0, ymax = 11)
    
    return




def annotateTimeUpdate(seis_maxesDepth,gps_maxes, timestamp,dt_end, dt_end2, timedelta,color_warn,color_warn2):
    
    seis_maxesDepth[1].annotate(timestamp, xy=(dt_end2+timedelta(0), 0), xytext=(10,250), textcoords='offset points', horizontalalignment='left', fontsize=20, rotation="vertical", color=color_warn2)
    
    seis_maxesDepth[1].annotate(timestamp, xy=(dt_end2+timedelta(0), 0), xytext=(10,700), textcoords='offset points', horizontalalignment='left', fontsize=20, rotation="vertical", color=color_warn2)
    
    seis_maxesDepth[0].set_ylabel("Depth [km]", fontsize=20, labelpad=4)

    gps_maxes[1].annotate(timestamp, xy=(dt_end+timedelta(0), 0), xytext=(10,50), textcoords='offset points', horizontalalignment='left', fontsize=20, rotation="vertical", color=color_warn)
      
    return
    
    
def setLandmarks(seis_axis, seis_axisLat):
    
    
    #Landmarks.. Will be stored with configparser later on as text
    seis_axis.text(0.01, 0.85, "Husavik", fontsize= 23, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
    seis_axis.text(0.01, 0.420, "Gjogurta", fontsize= 23, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
    seis_axis.text(0.01, 0.16, "Eyjafjardarall", fontsize= 23, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
    seis_axis.text(0.01, 0.62, "Flatey", fontsize= 23, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
    
    
    seis_axisLat.text(0.01, -0.98, "Husavik", fontsize= 23, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
    seis_axisLat.text(0.0051, -0.8, "Gjogurta", fontsize= 23, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
    seis_axisLat.text(0.01, -0.22, "Eyjafjardarall", fontsize= 23, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
    
    return
    
    
def setHorizLines(seis_maxes, seis_maxesLat, Q2, dt_end2, dt_start2):
    
    #Longitude horizontal lines, y will be stored in configparser, later. 
    seis_maxes[0].hlines(y= -17.34, xmin=dt_start2, xmax=dt_end2, color='gray', zorder=1)
    seis_maxes[0].hlines(y= -17.81, xmin=dt_start2, xmax=dt_end2, color='gray', zorder=1)
    seis_maxes[0].hlines(y= -18.72, xmin=dt_start2, xmax=dt_end2, color='gray', zorder=1)
    seis_maxes[0].hlines(y= -18.20, xmin=dt_start2, xmax=dt_end2, color='gray', zorder=1)
        
    #Latitude horizontal lines
    seis_maxesLat[1].hlines(y= 66.047, xmin=dt_start2, xmax=dt_end2, color='gray', zorder=1)
    seis_maxesLat[1].hlines(y= 66.173, xmin=dt_start2, xmax=dt_end2, color='gray', zorder=1)
    seis_maxesLat[1].hlines(y= 66.47,  xmin=dt_start2, xmax=dt_end2, color='gray', zorder=1)
    
    return
    
    
def setUpdateTime():
    
    return
    

def setSeisColors(Seis):
   
    Seis['Mcolors'] = float('nan')
    Seis.loc[  Seis['MLW'] < 2 , 'Mcolors'] = ['green']
    Seis.loc[ ( Seis['MLW'] >= 2 ) & ( Seis['MLW'] < 3 ) , 'Mcolors'] = 'gold'
    Seis.loc[ ( Seis['MLW'] >= 3 ) & ( Seis['MLW'] < 4 ) , 'Mcolors'] = 'darkorange'
    Seis.loc[ ( Seis['MLW'] >= 4 ) & ( Seis['MLW'] < 5 ) , 'Mcolors'] = 'orangered'
    Seis.loc[ ( Seis['MLW'] >= 5 ) & ( Seis['MLW'] < 6 ) , 'Mcolors'] = 'red'
    Seis.loc[ ( Seis['MLW'] >= 6 ), 'Mcolors'] = 'darkred'
    return  Seis['Mcolors'] 
    
    
    
def setVerticalUpdateLines(seis_maxes,seis_maxesLat,seis_maxesDepth ,gps_maxes,dt_end, dt_end2 ):
     #Time update vertical lines
    seis_maxes[0].axvline(dt_end2, color='gray')
    seis_maxesLat[0].axvline(dt_end2, color='gray')
    seis_maxesDepth[0].axvline(dt_end2, color='gray')
    gps_maxes[0].axvline(dt_end, color='gray')
    
    return



def setColorWarn(timestamp, dt_end, timedelta):
    
    if (timestamp - dt_end) > timedelta(minutes=15):
        color_warn="r"
    else:
        color_warn="g"
    
    return color_warn
    
    

def setpatch(seis_maxes):
    
    #markSize = [22.5, 20, 17.5, 15, 12.5, 10] 
    patchColors = ["darkred", "red","orangered","darkorange","gold","green"]
    texts = ["6+", "5-6", "4-5", "3-4", "2-3", "0-2"]
    
    patches = [ plt.plot([],[], marker="o", ms=25,  mec='black', color=patchColors[i], 
            label="{:s}".format(texts[i]))[0]  for i in range(len(texts)) ]
    
    seis_maxes[0].legend(facecolor=None, framealpha=0, handles=patches,bbox_to_anchor=(0.15, 0.87), 
           loc='center', ncol=2, numpoints=1 ,fontsize = 20)
    
    return





