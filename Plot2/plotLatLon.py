#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function


from pylab import *
import numpy as np
import geofunc.geo as geo
import datetime

from datetime import datetime
from datetime import datetime


from matplotlib.colors import LinearSegmentedColormap

from matplotlib import cm
import matplotlib
matplotlib.pyplot.stem
matplotlib.axes.Axes.stem



def plotFig(num1, num2, timeRange1, timeRange2, qAccuracy):
    """
    """
    import cparser
    import logging

    from pathlib import Path
    from datetime import datetime as dt
    import gtimes.timefunc as tf
    #import timesmatplt.timesmatplt as tplt  # plot GPS data?
    import numpy as np # some simple calculations
    import geofunc.geo as geo
    import pandas as pd

    import geo_dataread.sil_read as gdrsil
    import matplotlib.dates as mdates  # better date/time axis ticks
    import timesmatplt.timesmatplt as tplt  # plot GPS data?

    # must register pandas datetime converter in matplotlib to avoid warning
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()
    
    import matplotlib.patches as mpatches
    from gtimes.timefunc import TimetoYearf
    
    import geo_dataread.gas_read as gdrgas
    import geo_dataread.sil_read as gdrsil
    import geo_dataread.hytro_read as gdrhyt
    import geo_dataread.gps_read as gdrgps
    import matplotlib.lines as mlines
    import functions as ft
    from datetime import timedelta
    
    
    import multi10July as ml
    
    
    now = datetime.now()
    
    dstr="%Y-%m-%d %H:%M:%S"
    dfstr="%Y-%m-%d"  # date format
    ### INPUT:
    sid = "GJOG"
    
    
    #Create the first time period
    start = tf.currDatetime(num1)
    start_date = dt.strftime(start, dstr)

    if num2 == 0:
        end_date=dt.strftime(dt.now(), dstr)
    else:
        end_date = tf.currDatetime(num2)
    
    
    #Create the second time period
    start2 = tf.currDatetime(timeRange1)
    start_date2 = dt.strftime(start2, dstr)
    
    if timeRange2 == 0:
        end_date2=dt.strftime(dt.now(), dstr)
    else:
        end_date2 = tf.currDatetime(timeRange2)
     
    logging_level=logging.INFO
    ## Everything else from here is automatic:

    StaPars = cparser.Parser()

    dt_start = tf.toDatetime(start_date,dstr) # datetime obj
    dt_end = tf.toDatetime(end_date,dstr)
    f_start = tf.currYearfDate(refday=dt_start) # float (fractional year)
    f_end = tf.currYearfDate(refday=dt_end)
    
    dt_start2 = tf.toDatetime(start_date2,dstr) # datetime obj
    dt_end2 = tf.toDatetime(end_date2,dstr)
    f_start2 = tf.currYearfDate(refday=dt_start2) # float (fractional year)
    f_end2 = tf.currYearfDate(refday=dt_end2)
   
    timestamp_format="%a %-d.%b %Y, %H:%M"
    timestamp=dt.now()
  
    
    pre_path= "/mnt_data/"
    
    ## Get GPS data with some basic filtering
    gps_path="gpsdata/"
    #-#detrend_period=[TimetoYearf(2001,1,1), TimetoYearf(2010,1,1)] # FOR HVOL
    #-#detrend_line=[TimetoYearf(2010,6,1), TimetoYearf(2014,4,1)] # FOR HVOL
    ## Get GPS data with some basic filtering
    detrend_period=[TimetoYearf(2001,1,1), TimetoYearf(2019,1,1)]
    detrend_line=[TimetoYearf(2010,6,1), TimetoYearf(2019,4,1)]

    #GPS = gdrgps.read_gps_data(sid, Dir=pre_path+gps_path, start=dt_start, end=dt_end, detrend_period=detrend_period, 
    #               detrend_line=detrend_line, logging_level=logging_level)
    GPS = gdrgps.read_gps_data(sid, Dir=pre_path+gps_path, start=dt_start, end=dt_end, detrend_period=None, 
                   detrend_line=None, logging_level=logging_level)

    ## Get seismicity data
    sil_path="sildata/"
    #filtering the seismisity
    
    constr = {  #"start": start,     "end": end, 
                "min_lat": 66.00,  "max_lat":  66.6,
                "min_lon": -18.8, "max_lon": -17.2,
                "min_Q": 30.0, "max_Q": None,  
                #"min_depth": 15.0, "max_depth": None, 
                #"min_mlw": 1.0,  "max_mlw": None,
    }
    
    col_names = ['latitude', 'Dlatitude', 'longitude', 'Dlongitude', 
                    'depth', 'Ddepth', 'MLW', 'seismic_moment', 'cum_moment', 'Q']
    
    # I think we need this
    # For first time period            <this>       <this>
    Seis = gdrsil.read_sil_data(start=dt_start, end=dt_end, base_path=pre_path+sil_path,  
            fread="default", aut=True, aut_per=2, rcolumns=col_names, cum_moment=True, logger='default', **constr)
    
    #For second time period             <this>         <this>   
    Seis2 = gdrsil.read_sil_data(start=dt_start2, end=dt_end2, base_path=pre_path+sil_path,  
            fread="default", aut=True, aut_per=2, rcolumns=col_names, cum_moment=True, logger='default', **constr)

   
    loc = "IMO"
    if loc=="IMO":
        conn_dict={ 
               "dbname"    : "gas",
               "user"      : "gas_read",
               "password"  : "qwerty7",
               "host"      : "dev.psql.vedur.is",
               "port"      :  5432
               }
    
    elif loc=="home":
        # runn to make this work
        #ssh -f gpsops@rek.vedur.is -L 5431:dev.psql.vedur.is:5432 -N
        conn_dict={
                "dbname"    : "gas",
                "user"      : "gas_read",
                "password"  : "qwerty7",
                "host"      : "localhost",
                "port"      :  5431
                }
    
    
    import matplotlib.pyplot as plt
    if num2 == 0:
        pend = timestamp + (dt_end - dt_start)/40
    else:
        pend = dt_end + (dt_end - dt_start)/40
    if timeRange2 == 0:
        pend2 = timestamp + (dt_end2 - dt_start2)/40
    else:
        pend2 = dt_end2 + (dt_end2 - dt_start2)/40
        
        
        
    #pend2 = dt_end2 + (dt_end2 - dt_start2)/40
  
    #pend = timestamp + (dt_end - dt_start)/20
    #pend2 = dt_end2 + (dt_end2 - dt_start2)/20
    
    ##############################################################
    ##############################################################
 
    Title="Graf gert: {}".format( timestamp.strftime(timestamp_format) )
    
    
    depth = True
    fig = ft.fourFrame(Ylabel=None, Title=Title, depth = depth)
    fig = ft.tstwofigTickLabelsFourFrame(fig,period=(pend-dt_start),period2=(pend2-dt_start2))
    
    
    supTitle="Jardskjalftamaelingar vid Tjornes"
    fig.suptitle(supTitle, fontsize=40, verticalalignment="center",x = 0.813,y = 0.935)
    
    Seis['Mcolors'] = ml.setSeisColors(Seis)
    Seis2['Mcolors'] = ml.setSeisColors(Seis2)
      
    Seis['Medge'] = float('nan')
    Seis.loc[ Seis["Q"] > 95, 'Medge' ] = 'black'
    Seis.loc[ Seis["Q"] <= 95, 'Medge' ] = 'face'
    
    Q = Seis[ Seis["Q"] > qAccuracy]
    Y = Seis[ Seis["Q"] <= qAccuracy]
    
    Q2 = Seis2[ Seis2["Q"] > qAccuracy]
    Y2 = Seis2[ Seis2["Q"] <= qAccuracy]

  
    color_warn = ml.setColorWarn(timestamp, dt_end, timedelta)
    color_warn2 = ml.setColorWarn(timestamp, dt_end2, timedelta)
    
    
    
    # setting upp the axis stuff 
    seis_axis = fig.axes[0]
    seis_maxes = [seis_axis, seis_axis.twinx()]
    seis_axis.set_xlim([dt_start2, dt_end2])
    
    #Set a date title on the plot with gren/red color update 
    seis_maxes[0].set_title(Title, color=color_warn2, verticalalignment="bottom")
    
    seis_axisLat = fig.axes[1]
    seis_maxesLat = [seis_axisLat, seis_axisLat.twinx()]
    seis_axisLat.set_xlim([dt_start2, dt_end2])
    
    seis_axisDepth = fig.axes[2]
    seis_maxesDepth = [seis_axisDepth, seis_axisDepth.twinx()]
    seis_axisDepth.set_xlim([dt_start2, dt_end2])
    
    gps_axis = fig.axes[3]
    gps_maxes = [gps_axis, gps_axis.twinx(), gps_axis.twinx(),gps_axis.twinx()] 
    gps_axis.set_xlim([dt_start, dt_end])

    gps_maxes[2].spines['right'].set_position(("axes", 1.2))
    gps_maxes[2].set_frame_on(True)
    gps_maxes[2].patch.set_visible(True)
    
    fig.subplots_adjust(right=1.5)
    
    #Eartquake magnitude color label on the top graf
    ml.setpatch(seis_maxes)
    
    #the latest earthquakes in the database
    end = Y.index.max()
    end2 = Y2.index.max()
    
    """
    timestamp2 = Y.index.max()
    str3 = timestamp2.strftime(timestamp_format)
    
    end = Y.index.max()
    temp = str(end)
    """
    #Just testing
    temp = Q2.index.max()
    print("Testl;ine")
    print(Q2.index.max())
    print(Y2.index.max())
    print(Q2['latitude'][-1])
    print(Q2.index[-1])
    #landmarks and line stuff
    ml.setLandmarks(seis_axis, seis_axisLat)
    
    ml.setHorizLines(seis_maxes, seis_maxesLat, Q2, dt_end2, dt_start2) 
    ml.setVerticalUpdateLines(seis_maxes, seis_maxesLat, seis_maxesDepth , gps_maxes,dt_end, dt_end2 )
    
    
    #### Plots of Longitude, Latitude, Depth, Gps and Seismic moment
    #   Q2 and Y2 for second time period 
    ml.plotLon(Q2, Y2, seis_maxes, constr,seis_axis , dt_start2, pend2)
    ml.plotLat(Q2, Y2, seis_maxesLat, dt_start2, pend2)
    ml.plotDepth(Q2, Y2, seis_maxesDepth, dt_start2, pend2)
    ml.plotGps(GPS,gps_maxes, gps_axis, start, pend, sid)
    ml.plotMomentAndcolorCodeQukes(Seis, start, pend, gps_maxes)
     
    #Set date and color coded update, green/red
    ml.annotateTimeUpdate(seis_maxesDepth, gps_maxes, timestamp, dt_end, dt_end2, timedelta, color_warn, color_warn2)
    
    # write to image file
    home = Path.home()
    print(home)
    
    print("Testing new beta")
    relpath = "multiplot/figures/"
    filebase = "GjogPlot4"
    filename = home.joinpath(relpath,filebase).as_posix()
    #filename = "/home/gpsops/multiplot/figures/multiplot"
  
    tplt.saveFig(filename, "png", fig)
    #tplt.saveFig(filename, "pdf", fig)
    
    print("Everthing is done")
    del fig

plotFig(-30,0,-2.9, 0, 95)

    