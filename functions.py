def spltbbfourFrame(Ylabel=None, Title=None):
    """
        Ylabel, 
        Title,

    output:
        fig, Figure object.

    """
    #import matplotlib.image as image

    from gtimes.timefunc import currTime
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    mpl.style.use('classic')

    
    # constructing a figure with three axes and adding a title
    multibl=1.3
    fig, axes = plt.subplots(nrows=4, ncols=1, figsize=(20*multibl,23*multibl))
    fig.subplots_adjust(hspace=0.1)

    
    if type(Title) is list:
        plt.suptitle(Title[0], y=0.965,x=0.51)
        axes[0].set_title(Title[1],y=1.01)
    elif type(Title) is str:
        axes[0].set_title(Title)
    else:
        pass


    if Ylabel == None: #
        Ylabels = ("Y1","Y2","Y3","Y4")


    # --- apply custom stuff to the whole fig ---
    plt.minorticks_on
    plt.gcf().autofmt_xdate(rotation=0, ha='left')
    mpl.rcParams['legend.handlelength'] = 0    
    mpl.rcParams['text.usetex'] = True
    mpl.rc('text.latex', preamble=r'\usepackage{color}')
    
    for i in range(4):
        axes[i].set_ylabel(Ylabel, fontsize='x-large', labelpad=0)
        axes[i].axhline(0,color='black') # zero line

    for ax in axes[-1:-5:-1]:
    #tickmarks lables and other and other axis stuff on each axes 
    # needs improvement
    
    # --- X axis ---
        xax = ax.get_xaxis()
    
        xax.set_tick_params(which='minor', direction='in', length=4, top='off')
        xax.set_tick_params(which='major', direction='in', length=10, top='off')

        if ax is axes[0]: # special case of top axes
            xax.set_tick_params(which='minor', direction='in', length=4, top='off')
            xax.set_tick_params(which='major', direction='in', length=10, top='off')
        #else:
        #    ax.spines['top'].set_visible(False)


    return fig


def tsfourfigTickLabels(fig,period=None):
    """
    """
    
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    from datetime import timedelta
    import timesmatplt.timesmatplt as tplt
    import timesmatplt.gasmatplt as gplt

    #minorLoc, minorFmt, majorLoc,majorFmt = tsLabels(period)
    
    print(period)
    axes = fig.axes
    
    # major labels in separate layer
    axes[-1].get_xaxis().set_tick_params(which='major', labelbottom='off') 
    axes[-1].get_xaxis().set_tick_params(which='major', pad=10) 


    for ax in axes[-1:-5:-1]:
        #tickmarks lables and other and other axis stuff on each axes 
        # needs improvement
        ax.grid(True)
        ax.grid(True,linestyle='solid',axis='x')
        if period < timedelta(2600):
            if ax is not axes[-1]:
                ax.grid(True, which='minor',axis='x',)
                ax.grid(False, which='major',axis='x',)
    
        # --- X axis ---
        xax = ax.get_xaxis()


        xax = tplt.tslabels(xax,period=period,locator='minor',formater='minor')
        xax = tplt.tslabels(xax,period=period,locator='minor',formater=None)
        xax = tplt.tslabels(xax,period=period,locator='major',formater=None)

        if ax is axes[-1]:
            xax = tplt.tslabels(xax,period=period,locator='major',formater='major')
            xax = tplt.tslabels(xax,period=period,locator='major',formater='minor')
        


        xax.set_tick_params(which='minor', direction='inout', length=4)
        xax.set_tick_params(which='major', direction='inout', length=15)

        for tick in xax.get_major_ticks():
            tick.label1.set_horizontalalignment('center')


        #for label in xax.get_ticklabels('major')[::]:
            #label.set_visible(False)
            #label.set_text("test")
         #   print "text: %s" % label.get_text()
            #xax.label.set_horizontalalignment('center')
    
        # --- Y axis ---
        yax = ax.get_yaxis()
        yax.set_minor_locator(mpl.ticker.AutoMinorLocator())
        yax.set_tick_params(which='minor', direction='in', length=4)
        yax.set_tick_params(which='major', direction='in', length=10)



    return fig



"""
spltbbthreeFrame and tstwofigTickLabels create three figures instead of four. 
"""
def spltbbthreeFrame(Ylabel=None, Title=None, depth = None):
    """
        Ylabel, 
        Title,

    output:
        fig, Figure object.

    """
    #import matplotlib.image as image

    from gtimes.timefunc import currTime
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    mpl.style.use('classic')
    #from matplotlib import gridspec
    from matplotlib import figure
    
    # constructing a figure with three axes and adding a title
    multibl=1.3
  
    #gridspec sets the y axis scale for the three different subplots
    if depth != None:
        fig, axes = plt.subplots(nrows=3, ncols=1,figsize=(20*multibl,23*multibl), gridspec_kw={'height_ratios': [23*multibl, 11.5*multibl, 23*multibl]} )
        plt.rc('font', size=14)
        print("this is strange")
    else:
        fig, axes = plt.subplots(nrows=3, ncols=1,figsize=(20*multibl,23*multibl))
    
    fig.subplots_adjust(hspace=0.1)
    #axes.set_title(Title, fontsize= 20) 
    if type(Title) is list:
        plt.suptitle(Title[0], y=0.865,x=0.651)
        axes[0].set_title(Title[1],y=1.01)
    elif type(Title) is str:
        axes[0].set_title(Title)
    else:
        pass

    
    if Ylabel == None: #
        Ylabels = ("Y1","Y2","Y3","Y4")

    
    # --- apply custom stuff to the whole fig ---
    plt.minorticks_on
    plt.gcf().autofmt_xdate(rotation=0, ha='left')
    mpl.rcParams['legend.handlelength'] = 0    
    mpl.rcParams['text.usetex'] = True
    mpl.rc('text.latex', preamble=r'\usepackage{color}')
    
    
    for i in range(3):  
        axes[i].set_ylabel(Ylabel, fontsize=20, labelpad=0)
        axes[i].axhline(0,color='black') # zero line

    for ax in axes[-1:-5:-1]:
    #tickmarks lables and other and other axis stuff on each axes 
    # needs improvement
    
    # --- X axis ---
        xax = ax.get_xaxis()
    
        xax.set_tick_params(which='minor', direction='in', length=4, top='off')
        xax.set_tick_params(which='major', direction='in', length=10, top='off')

        if ax is axes[0]: # special case of top axes
            xax.set_tick_params(which='minor', direction='in', length=4, top='off')
            xax.set_tick_params(which='major', direction='in', length=10, top='off')
        #else:
        #    ax.spines['top'].set_visible(False)

    
    return fig





def tsthreefigTickLabels(fig,period=None, period2 = None):
    """
    """

    import matplotlib.pyplot as plt
    import matplotlib as mpl
    from datetime import timedelta
    import timesmatplt.timesmatplt as tplt
    import timesmatplt.gasmatplt as gplt
    import numpy as np
    import matplotlib.ticker as mticker
    import matplotlib.dates as mdates
    #minorLoc, minorFmt, majorLoc,majorFmt = tsLabels(period)
    
    print("----------TESTING------------")
    axes = fig.axes
    
    
    
    
    # major labels in separate layer
    axes[-1].get_xaxis().set_tick_params(which='major', labelbottom='off') 
    axes[-1].get_xaxis().set_tick_params(which='major', pad=10) 

    count = 0
    
    for ax in axes[-1:-5:-1]:
        
        #tickmarks lables and other and other axis stuff on each axes 
        # needs improvement
        ax.grid(True)
        #ax.grid(True,linestyle='solid',axis='x')
        
        if period < timedelta(2600):
            if ax is not axes[-1]:
                ax.grid(True, which='minor',axis='x',)
                ax.grid(False, which='major',axis='x',)
        
        # --- X axis ---
        xax = ax.get_xaxis()
        
        ### This handles the possible different time periods for the graphs.
        ### first graph has a different period than th other two
        if ax is axes[0]:
            xax = tplt.tslabels(xax,period=period2,locator='minor',formater='minor')
            xax = tplt.tslabels(xax,period=period2,locator='minor',formater=None)
            xax = tplt.tslabels(xax,period=period2,locator='major',formater=None)
        else:
            xax = tplt.tslabels(xax,period=period,locator='minor',formater='minor')
            xax = tplt.tslabels(xax,period=period,locator='minor',formater=None)
            xax = tplt.tslabels(xax,period=period,locator='major',formater=None)
        
        
        if ax is axes[-1]:
            xax = tplt.tslabels(xax,period=period,locator='major',formater='major')
            xax = tplt.tslabels(xax,period=period,locator='major',formater='minor')
                
                             

        xax.set_tick_params(which='minor', direction='inout', length=4)
        xax.set_tick_params(which='major', direction='inout', length=15)
        
        
       
        for tick in xax.get_major_ticks():
            tick.label1.set_horizontalalignment('center')

       
        #for label in xax.get_ticklabels('major')[::]:
            #label.set_visible(False)
            #label.set_text("test")
         #   print "text: %s" % label.get_text()
            #xax.label.set_horizontalalignment('center')
    
        # --- Y axis ---
        yax = ax.get_yaxis()
        yax.set_minor_locator(mpl.ticker.AutoMinorLocator())
        yax.set_tick_params(which='minor', direction='in', length=4)
        yax.set_tick_params(which='major', direction='in', length=10)
        print(count)
        print(xax)
        print(yax)
        count+= 1
    
    print("finishd")
    return fig







"""
spltbbtwoFrame and tstwofigTickLabels create two figures instead of four. 
"""
def spltbbtwoFrame(Ylabel=None, Title=None):
    """
        Ylabel, 
        Title,

    output:
        fig, Figure object.

    """
    #import matplotlib.image as image

    from gtimes.timefunc import currTime
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    mpl.style.use('classic')

    
    # constructing a figure with three axes and adding a title
    multibl=1.3
    fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(20*multibl,23*multibl))
    fig.subplots_adjust(hspace=0.1)

    
    if type(Title) is list:
        plt.suptitle(Title[0], y=0.965,x=0.51)
        axes[0].set_title(Title[1],y=1.01)
    elif type(Title) is str:
        axes[0].set_title(Title)
    else:
        pass


    if Ylabel == None: #
        Ylabels = ("Y1","Y2","Y3","Y4")


    # --- apply custom stuff to the whole fig ---
    plt.minorticks_on
    plt.gcf().autofmt_xdate(rotation=0, ha='left')
    mpl.rcParams['legend.handlelength'] = 0    
    mpl.rcParams['text.usetex'] = True
    mpl.rc('text.latex', preamble=r'\usepackage{color}')
    
    for i in range(2):
        axes[i].set_ylabel(Ylabel, fontsize='x-large', labelpad=0)
        axes[i].axhline(0,color='black') # zero line

    for ax in axes[-1:-5:-1]:
    #tickmarks lables and other and other axis stuff on each axes 
    # needs improvement
    
    # --- X axis ---
        xax = ax.get_xaxis()
        
        xax.set_tick_params(which='minor', direction='in', length=4, top='off')
        xax.set_tick_params(which='major', direction='in', length=10, top='off')
        
        if ax is axes[0]: # special case of top axes
            xax.set_tick_params(which='minor', direction='in', length=4, top='off')
            xax.set_tick_params(which='major', direction='in', length=10, top='off')
        #else:
        #    ax.spines['top'].set_visible(False)


    return fig





def tstwofigTickLabels(fig,period=None):
    """
    """
    
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    from datetime import timedelta
    import timesmatplt.timesmatplt as tplt
    import timesmatplt.gasmatplt as gplt
    
    #minorLoc, minorFmt, majorLoc,majorFmt = tsLabels(period)
    
    axes = fig.axes
    
    # major labels in separate layer
    axes[-1].get_xaxis().set_tick_params(which='major', labelbottom='off') 
    axes[-1].get_xaxis().set_tick_params(which='major', pad=10) 


    for ax in axes[-1:-5:-1]:
        #tickmarks lables and other and other axis stuff on each axes 
        # needs improvement
        ax.grid(True)
        ax.grid(True,linestyle='solid',axis='x')
        if period < timedelta(2600):
            if ax is not axes[-1]:
                ax.grid(True, which='minor',axis='x',)
                ax.grid(False, which='major',axis='x',)
    
        # --- X axis ---
        xax = ax.get_xaxis()


        xax = tplt.tslabels(xax,period=period,locator='minor',formater='minor')
        xax = tplt.tslabels(xax,period=period,locator='minor',formater=None)
        xax = tplt.tslabels(xax,period=period,locator='major',formater=None)

        if ax is axes[-1]:
            xax = tplt.tslabels(xax,period=period,locator='major',formater='major')
            xax = tplt.tslabels(xax,period=period,locator='major',formater='minor')
        


        xax.set_tick_params(which='minor', direction='inout', length=4)
        xax.set_tick_params(which='major', direction='inout', length=15)

        for tick in xax.get_major_ticks():
            tick.label1.set_horizontalalignment('center')


        #for label in xax.get_ticklabels('major')[::]:
            #label.set_visible(False)
            #label.set_text("test")
         #   print "text: %s" % label.get_text()
            #xax.label.set_horizontalalignment('center')
    
        # --- Y axis ---
        yax = ax.get_yaxis()
        yax.set_minor_locator(mpl.ticker.AutoMinorLocator())
        yax.set_tick_params(which='minor', direction='in', length=4)
        yax.set_tick_params(which='major', direction='in', length=10)



    return fig






"""
spltbbtwoFrame and tstwofigTickLabels create two figures instead of four. 
"""

def spltbbtwoFrameTwoPriod(Ylabel=None, Title=None):
    """
        Ylabel, 
        Title,

    output:
        fig, Figure object.

    """
    #import matplotlib.image as image

    from gtimes.timefunc import currTime
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    mpl.style.use('classic')

    
    # constructing a figure with three axes and adding a title
    multibl=1.3
    fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(20*multibl,23*multibl))
    fig.subplots_adjust(hspace=0.1)

    
    if type(Title) is list:
        plt.suptitle(Title[0], y=0.965,x=0.51)
        axes[0].set_title(Title[1],y=1.01)
    elif type(Title) is str:
        axes[0].set_title(Title)
    else:
        pass


    if Ylabel == None: #
        Ylabels = ("Y1","Y2","Y3","Y4")


    # --- apply custom stuff to the whole fig ---
    plt.minorticks_on
    plt.gcf().autofmt_xdate(rotation=0, ha='left')
    mpl.rcParams['legend.handlelength'] = 0    
    mpl.rcParams['text.usetex'] = True
    mpl.rc('text.latex', preamble=r'\usepackage{color}')
    
    for i in range(2):
        axes[i].set_ylabel(Ylabel, fontsize='x-large', labelpad=0)
        axes[i].axhline(0,color='black') # zero line

    for ax in axes[-1:-5:-1]:
    #tickmarks lables and other and other axis stuff on each axes 
    # needs improvement
    
    # --- X axis ---
        xax = ax.get_xaxis()
        
        xax.set_tick_params(which='minor', direction='in', length=4, top='off')
        xax.set_tick_params(which='major', direction='in', length=10, top='off')
        
        if ax is axes[0]: # special case of top axes
            xax.set_tick_params(which='minor', direction='in', length=4, top='off')
            xax.set_tick_params(which='major', direction='in', length=10, top='off')
        #else:
        #    ax.spines['top'].set_visible(False)


    return fig





def tstwofigTickLabelsFourFrame(fig,period=None, period2 = None):
    """
    """
    
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    from datetime import timedelta
    import timesmatplt.timesmatplt as tplt
    import timesmatplt.gasmatplt as gplt
    
    #minorLoc, minorFmt, majorLoc,majorFmt = tsLabels(period)
    
    axes = fig.axes
    
    # major labels in separate layer
    axes[-1].get_xaxis().set_tick_params(which='major', labelbottom='off') 
    axes[-1].get_xaxis().set_tick_params(which='major', pad=10) 


    for ax in axes[-1:-5:-1]:
        #tickmarks lables and other and other axis stuff on each axes 
        # needs improvement
        ax.grid(True)
        ax.grid(True,linestyle='solid',axis='x')
        if period < timedelta(2600):
            if ax is not axes[-1]:
                ax.grid(True, which='minor',axis='x',)
                ax.grid(False, which='major',axis='x',)
    
        # --- X axis ---
        xax = ax.get_xaxis()
         ### This handles the possible different time periods for the graphs.
        ### first graph has a different period than th other two
        if ax is axes[0]:
            xax = tplt.tslabels(xax,period=period2,locator='minor',formater='minor')
            xax = tplt.tslabels(xax,period=period2,locator='minor',formater=None)
            xax = tplt.tslabels(xax,period=period2,locator='major',formater=None)
        else:
            xax = tplt.tslabels(xax,period=period,locator='minor',formater='minor')
            xax = tplt.tslabels(xax,period=period,locator='minor',formater=None)
            xax = tplt.tslabels(xax,period=period,locator='major',formater=None)
        
        
        if ax is axes[-1]:
            xax = tplt.tslabels(xax,period=period,locator='major',formater='major')
            xax = tplt.tslabels(xax,period=period,locator='major',formater='minor')
                
                             

        xax.set_tick_params(which='minor', direction='inout', length=4)
        xax.set_tick_params(which='major', direction='inout', length=15)
        
        
       
        for tick in xax.get_major_ticks():
            tick.label1.set_horizontalalignment('center')

       

        #for label in xax.get_ticklabels('major')[::]:
            #label.set_visible(False)
            #label.set_text("test")
         #   print "text: %s" % label.get_text()
            #xax.label.set_horizontalalignment('center')
    
        # --- Y axis ---
        yax = ax.get_yaxis()
        yax.set_minor_locator(mpl.ticker.AutoMinorLocator())
        yax.set_tick_params(which='minor', direction='in', length=4)
        yax.set_tick_params(which='major', direction='in', length=10,labelsize = 17)
        #yax.yticks(fontsize=30)
    return fig




"""
spltbbthreeFrame and tstwofigTickLabels create three figures instead of four. 
"""
def fourFrame(Ylabel=None, Title=None, depth = None):
    """
        Ylabel, 
        Title,

    output:
        fig, Figure object.

    """
    #import matplotlib.image as image

    from gtimes.timefunc import currTime
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    mpl.style.use('classic')
    #from matplotlib import gridspec
    from matplotlib import figure
    
    # constructing a figure with three axes and adding a title
    multibl=1.3
  
    #gridspec sets the y axis scale for the three different subplots
    if depth != None:
        fig, axes = plt.subplots(nrows=4, ncols=1,figsize=(20*multibl,23*multibl), gridspec_kw={'height_ratios': [23*multibl,23*multibl, 11.5*multibl, 23*multibl]} )
        plt.rc('font', size=20)
        print("this is strange")
    else:
        fig, axes = plt.subplots(nrows=3, ncols=1,figsize=(20*multibl,23*multibl))
    
    fig.subplots_adjust(hspace=0.1)
    #axes.set_title(Title, fontsize= 20) 
    if type(Title) is list:
        plt.suptitle(Title[0], y=0.865,x=0.651)
        axes[0].set_title(Title[1],y=1.01)
    elif type(Title) is str:
        axes[0].set_title(Title)
    else:
        pass

    
    if Ylabel == None: #
        Ylabels = ("Y1","Y2","Y3","Y4")

    
    # --- apply custom stuff to the whole fig ---
    plt.minorticks_on
    plt.gcf().autofmt_xdate(rotation=0, ha='left')
    mpl.rcParams['legend.handlelength'] = 0    
    mpl.rcParams['text.usetex'] = True
    mpl.rc('text.latex', preamble=r'\usepackage{color}')
    
    
    for i in range(4):  
        axes[i].set_ylabel(Ylabel, fontsize=16, labelpad=0)
        axes[i].axhline(0,color='black') # zero line

    for ax in axes[-1:-5:-1]:
    #tickmarks lables and other and other axis stuff on each axes 
    # needs improvement
    
    # --- X axis ---
        xax = ax.get_xaxis()
    
        xax.set_tick_params(which='minor', direction='in', length=4, top='off')
        xax.set_tick_params(which='major', direction='in', length=10, top='off')

        if ax is axes[0]: # special case of top axes
            xax.set_tick_params(which='minor', direction='in', length=4, top='off')
            xax.set_tick_params(which='major', direction='in', length=10, top='off')
        #else:
        #    ax.spines['top'].set_visible(False)
        
    
    return fig





def tsthreefigTickLabels(fig,period=None, period2 = None):
    """
    """

    import matplotlib.pyplot as plt
    import matplotlib as mpl
    from datetime import timedelta
    import timesmatplt.timesmatplt as tplt
    import timesmatplt.gasmatplt as gplt
    import numpy as np
    import matplotlib.ticker as mticker
    import matplotlib.dates as mdates
    #minorLoc, minorFmt, majorLoc,majorFmt = tsLabels(period)
    
    print("----------TESTING------------")
    axes = fig.axes
    
    
    
    
    # major labels in separate layer
    axes[-1].get_xaxis().set_tick_params(which='major', labelbottom='off') 
    axes[-1].get_xaxis().set_tick_params(which='major', pad=10) 

    count = 0
    
    for ax in axes[-1:-5:-1]:
        
        #tickmarks lables and other and other axis stuff on each axes 
        # needs improvement
        ax.grid(True)
        #ax.grid(True,linestyle='solid',axis='x')
        
        if period < timedelta(2600):
            if ax is not axes[-1]:
                ax.grid(True, which='minor',axis='x',)
                ax.grid(False, which='major',axis='x',)
        
        # --- X axis ---
        xax = ax.get_xaxis()
        
        ### This handles the possible different time periods for the graphs.
        ### first graph has a different period than th other two
        if ax is axes[0]:
            xax = tplt.tslabels(xax,period=period2,locator='minor',formater='minor')
            xax = tplt.tslabels(xax,period=period2,locator='minor',formater=None)
            xax = tplt.tslabels(xax,period=period2,locator='major',formater=None)
        else:
            xax = tplt.tslabels(xax,period=period,locator='minor',formater='minor')
            xax = tplt.tslabels(xax,period=period,locator='minor',formater=None)
            xax = tplt.tslabels(xax,period=period,locator='major',formater=None)
        
        
        if ax is axes[-1]:
            xax = tplt.tslabels(xax,period=period,locator='major',formater='major')
            xax = tplt.tslabels(xax,period=period,locator='major',formater='minor')
                
                             

        xax.set_tick_params(which='minor', direction='inout', length=4)
        xax.set_tick_params(which='major', direction='inout', length=15)
        
        
       
        for tick in xax.get_major_ticks():
            tick.label1.set_horizontalalignment('center')

       
        #for label in xax.get_ticklabels('major')[::]:
            #label.set_visible(False)
            #label.set_text("test")
         #   print "text: %s" % label.get_text()
            #xax.label.set_horizontalalignment('center')
    
        # --- Y axis ---
        yax = ax.get_yaxis()
        yax.set_minor_locator(mpl.ticker.AutoMinorLocator())
        yax.set_tick_params(which='minor', direction='in', length=4)
        yax.set_tick_params(which='major', direction='in', length=10)
        print(count)
        print(xax)
        print(yax)
        count+= 1
    
    print("finishd")
    return fig











###########################################################
###########################################################
###########################################################
########                                          #########
########    Not Using this code, just testing     #########
########                                          #########
###########################################################
###########################################################
###########################################################

"""
spltbbtwoFrame and tstwofigTickLabels create two figures instead of four. 
"""
def xPlotTesttwoFrame(Ylabel=None, Title=None):
    """
        Ylabel, 
        Title,

    output:
        fig, Figure object.

    """
    #import matplotlib.image as image

    from gtimes.timefunc import currTime
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    mpl.style.use('classic')

    
    # constructing a figure with three axes and adding a title
    multibl=1.3
    fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(20*multibl,23*multibl))
    fig.subplots_adjust(hspace=0.1)

    
    if type(Title) is list:
        plt.suptitle(Title[0], y=0.965,x=0.51)
        axes[0].set_title(Title[1],y=1.01)
    elif type(Title) is str:
        axes[0].set_title(Title)
    else:
        pass


    if Ylabel == None: #
        Ylabels = ("Y1","Y2","Y3","Y4")


    # --- apply custom stuff to the whole fig ---
    plt.minorticks_on
    plt.gcf().autofmt_xdate(rotation=0, ha='left')
    mpl.rcParams['legend.handlelength'] = 0    
    mpl.rcParams['text.usetex'] = True
    mpl.rc('text.latex', preamble=r'\usepackage{color}')
    
    for i in range(2):
        axes[i].set_ylabel(Ylabel, fontsize='x-large', labelpad=0)
        axes[i].axhline(0,color='black') # zero line

    for ax in axes[-1:-5:-1]:
    #tickmarks lables and other and other axis stuff on each axes 
    # needs improvement
    
    # --- X axis ---
        xax = ax.get_xaxis()
        
        xax.set_tick_params(which='minor', direction='in', length=4, top='off')
        xax.set_tick_params(which='major', direction='in', length=10, top='off')
        
        if ax is axes[0]: # special case of top axes
            xax.set_tick_params(which='minor', direction='in', length=4, top='off')
            xax.set_tick_params(which='major', direction='in', length=10, top='off')
        #else:
        #    ax.spines['top'].set_visible(False)


    return fig





def xPlotTestTickLabels(fig,period=None):
    """
    """
    
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    from datetime import timedelta
    import timesmatplt.timesmatplt as tplt
    import timesmatplt.gasmatplt as gplt
    
    #minorLoc, minorFmt, majorLoc,majorFmt = tsLabels(period)
    
    axes = fig.axes
    
    # major labels in separate layer
    axes[-1].get_xaxis().set_tick_params(which='major', labelbottom='off') 
    axes[-1].get_xaxis().set_tick_params(which='major', pad=10) 


    for ax in axes[-1:-5:-1]:
        #tickmarks lables and other and other axis stuff on each axes 
        # needs improvement
        ax.grid(True)
        ax.grid(True,linestyle='solid',axis='x')
        if period < timedelta(2600):
            if ax is not axes[-1]:
                ax.grid(True, which='minor',axis='x',)
                ax.grid(False, which='major',axis='x',)
    
        # --- X axis ---
        xax = ax.get_xaxis()


        xax = tplt.tslabels(xax,period=period,locator='minor',formater='minor')
        xax = tplt.tslabels(xax,period=period,locator='minor',formater=None)
        xax = tplt.tslabels(xax,period=period,locator='major',formater=None)

        if ax is axes[-1]:
            xax = tplt.tslabels(xax,period=period,locator='major',formater='major')
            xax = tplt.tslabels(xax,period=period,locator='major',formater='minor')
        


        xax.set_tick_params(which='minor', direction='inout', length=4)
        xax.set_tick_params(which='major', direction='inout', length=15)

        for tick in xax.get_major_ticks():
            tick.label1.set_horizontalalignment('center')


        #for label in xax.get_ticklabels('major')[::]:
            #label.set_visible(False)
            #label.set_text("test")
         #   print "text: %s" % label.get_text()
            #xax.label.set_horizontalalignment('center')
    
        # --- Y axis ---
        yax = ax.get_yaxis()
        yax.set_minor_locator(mpl.ticker.AutoMinorLocator())
        yax.set_tick_params(which='minor', direction='in', length=4)
        yax.set_tick_params(which='major', direction='in', length=10)



    return fig





