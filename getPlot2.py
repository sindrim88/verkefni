import argparse
import choosePlot as Plot

import argparse
import geofunc.geofunc as geo
import numpy as np
import pandas as pd 

from configparser import ConfigParser
file = 'config.ini'
config = ConfigParser()
config.read(file)


def main():
    
    parser = argparse.ArgumentParser(description="Graphing earthquakes, gps coordinates program",add_help=False)
    #parser.add_argument('--p',  type=str, help="Type in the name of the gps station in CAPITAL LETTERS")
    
    """
    Use uxample: --a 1 --a 2 --a 3 ect
    parser.add_argument('--a', action='append', dest='collection',default=[], type=float,
                    help='Add repeated values to a list',)
    x = args.collection
    """
    
    #Time perod parsing first period is mandatory... Maybe
    #
    parser.add_argument('x', type=int, nargs=1, help="Type in time period, e.g -100 0")
    parser.add_argument('y', type=int, nargs=1, help="Type in time period, e.g -100 0")
    
    #parser.add_argument('--O', action='store', type=int, nargs=2, help="Type in time period, e.g --one -100 0")
    parser.add_argument('--T', action='store', type=int, nargs=2, help="Type in two time periods , e.g --T -100 0 -5 0")
    parser.add_argument('S',type=str,help="Type in the name of the gps station in CAPITAL LETTERS")
    parser.add_argument('--N', action='store', type=int, nargs=1, help="Type in the number of figures to plot, e.g --N 4. 2,3,4       #are valid fig numbers")
    
    args = parser.parse_args()
    
    #input1 = args.O
    #p = args.p
    
    fig_num = args.N
    s = args.S
    input2 = args.T
    x = args.x
    y = args.y
    

    
    ##### Old implementation for selecting plots. Probably not needed.
    """
    if input1:
        if fig_num[0] == 2:
            print("One timeframe and two figs")
            Plot.twoPlot(input1[0],input1[1], s) 
            
        elif fig_num[0] == 4:
            print("One timeframe and four figs")
            Plot.fourPlot(input1[0],input1[1], s)
    elif input2:
        if fig_num[0] == 3:
            print("Two timeframes and three figs")
            Plot.threePlot(input2[0],input2[1],input2[2],input2[3], s)
    """
    #####
    
    
    # need to find out if the selected system has gas/hytro data.
    checkGas = None
    if config.get(s,'isGas') == 'true':
        checkGas = True
    else:
        checkGas = False
        
    # Testing
    print(checkGas)
    print(config.get(s,'isGas'))
    
    
    
    #Converting/Casting the input parser N from a string to a list of int if it is not None
    #Becomes a list with 1 element, from there it's easy to use as an Integer or float.
    if fig_num != None:
        map_object = map(int, fig_num)
        lst = list(map_object)
        num = lst[0]
    
    
    ### Select the appropriate plots based on date input from parser, if the system has 
    ### gas/hytro data in config file and/or if N has been selected
    
    if input2 == None:
        if checkGas and fig_num == None:
            print("One timeframe and four figs")
            Plot.fourPlot(x[0], y[0], s)
            
        elif checkGas and num == 2:
            
            print("One timeframe and two figs")
            Plot.twoPlot(x[0], y[0], s)
        else:
            Plot.twoPlot(x[0], y[0], s)
    elif input2:
        print("Two timeframes and three figs")
        Plot.threePlot(x[0], y[0], input2[0],input2[1], s)
    
    
    print("closing time")
    


if __name__=="__main__":
    main()

    